package com.pustovit.hellowword.fragments.news.di.news

import com.pustovit.hellowword.fragments.news.domain.NewsRepository
import com.pustovit.hellowword.fragments.news.presentation.NewsFragmentViewModel
import com.pustovit.hellowword.fragments.news.presentation.NewsFragmentViewModelFactory
import dagger.Module
import dagger.Provides
import timber.log.Timber
import javax.inject.Singleton

@Module
class NewsModule {

    @NewsScope
    @Provides
    fun provideNewsViewModelFactory(repository: NewsRepository):NewsFragmentViewModelFactory{
        Timber.d("provideNewsViewModelFactory called")
        return NewsFragmentViewModelFactory(repository)
    }
}