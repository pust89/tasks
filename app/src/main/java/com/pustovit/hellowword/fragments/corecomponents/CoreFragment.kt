package com.pustovit.hellowword.fragments.corecomponents

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.pustovit.hellowword.MainActivity
import com.pustovit.hellowword.R
import com.pustovit.hellowword.common.BaseFragment
import com.pustovit.hellowword.fragments.corecomponents.SelectSongFragment.Companion.EXTRA_SONG_FROM_LIST
import com.pustovit.hellowword.fragments.corecomponents.service.PlayerService
import com.pustovit.hellowword.navigation.Screens
import kotlinx.android.synthetic.main.fragment_core.*
import timber.log.Timber

class CoreFragment : BaseFragment() {

    companion object {
        fun newInstance() = CoreFragment()
        const val BROADCAST_ACTION = "com.pustovit.BROADCAST"
        const val BROADCAST_ACTION_SELECT_SONG = "com.pustovit.BROADCAST_SELECT_SONG"
    }

    private var selectedSong: Song? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_corev2, container, false)
    }


    override fun getTitleResId(): Int {
        return R.string.title_core_fragment
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        selectedSong = preferences.getSongFromSharedPreferences(requireContext())

        registerReceiver()

        /*Fix bug with redirection*/
        activity?.intent?.apply {
            this.action = null
        }
    }


    override fun onStop() {
        super.onStop()
        selectedSong?.let {
            preferences.saveSongToSharedPreferences(requireContext(), it)
        }
    }


    override fun onDestroy() {
        unregisterReceiver()
        super.onDestroy()
    }


    override fun onResume() {
        super.onResume()
        selectedSong?.apply { selectSong(this) }
        Timber.d("onResume: selectedSong = ${selectedSong.toString()}")

        btnPlay.setOnClickListener { playSong(selectedSong) }
        btnPause.setOnClickListener { pauseSong() }
        btnStop.setOnClickListener { stopSong() }

        btnSelectSong.setOnClickListener {
            router.navigateTo(Screens.SelectSongFragmentScreen())
        }
    }

    private var flagNeedToPlayNow: Boolean = false

    private val coreFragmentBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {

            intent?.let {

                when (intent.action ?: "") {
                    BROADCAST_ACTION -> {

                        intent.getParcelableExtra<Song>(PlayerService.EXTRA_SONG)?.apply {
                            preferences.saveSongToSharedPreferences(
                                this@CoreFragment.requireContext(),
                                this)
                        }

                    }

                    BROADCAST_ACTION_SELECT_SONG -> {
                        intent.getParcelableExtra<Song>(EXTRA_SONG_FROM_LIST)?.apply {
                            selectedSong = this
                            Timber.d("Broadcast: selectedSong = ${selectedSong.toString()}")
                            router.exit()
                            flagNeedToPlayNow = true
                        }
                    }
                    else -> {}//Nothing
                }
            }
        }
    }

    private fun displaySongInfo(song: Song) {
        tvSong.text = song.title
        tvArtist.text = song.artist
        tvGenre.text = song.genre
    }

    private fun selectSong(song: Song) {
        selectedSong = song.also {
            displaySongInfo(it)
            preferences.saveSongToSharedPreferences(requireContext(), it)
            if (flagNeedToPlayNow) {
                playSong(it)
                flagNeedToPlayNow = false
            }
        }
    }

    private fun playSong(selectedSong: Song?) {
        Timber.d("playSong: selectedSong = $selectedSong")
        if (selectedSong != null) {
            activity?.startService(PlayerService.getIntentPlaySong(selectedSong))
        } else {
            Toast.makeText(requireContext(), "You don't select a song!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun pauseSong() {
        if (PlayerService.isActive) activity?.startService(PlayerService.getIntentPauseSong())
    }

    private fun stopSong() {
        if (PlayerService.isActive) activity?.startService(PlayerService.getIntentStopSong())
    }

    private fun registerReceiver() {
        requireActivity().registerReceiver(coreFragmentBroadcastReceiver, IntentFilter().apply {
            addAction(BROADCAST_ACTION_SELECT_SONG)
            addAction(BROADCAST_ACTION)
        })
    }

    private fun unregisterReceiver() {
        requireActivity().unregisterReceiver(coreFragmentBroadcastReceiver)
    }
}