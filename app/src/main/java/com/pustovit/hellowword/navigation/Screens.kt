package com.pustovit.hellowword.navigation


import androidx.fragment.app.Fragment
import com.pustovit.hellowword.fragments.corecomponents.CoreFragment
import com.pustovit.hellowword.fragments.corecomponents.SelectSongFragment
import com.pustovit.hellowword.fragments.customview.CustomViewFragment
import com.pustovit.hellowword.fragments.edittext.EditTextFragment
import com.pustovit.hellowword.fragments.googlemaps.MapsFragment
import com.pustovit.hellowword.fragments.hello.HelloFragment
import com.pustovit.hellowword.fragments.home.HomeFragment
import com.pustovit.hellowword.fragments.news.domain.News
import com.pustovit.hellowword.fragments.news.presentation.NewsFragment
import com.pustovit.hellowword.fragments.news.presentation.SelectedNewsFragment
import com.pustovit.hellowword.fragments.orientationtablet.OrientationTabletFragment
import com.pustovit.hellowword.fragments.recview.FigureRecyclerViewFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {

    class EditTextFragmentScreen : SupportAppScreen() {

        override fun getFragment(): Fragment {
            return EditTextFragment.newInstance()
        }
    }

    class RecyclerFragmentScreen : SupportAppScreen() {
        override fun getFragment(): Fragment? {
            return FigureRecyclerViewFragment.newInstance()
        }
    }

    class HelloFragmentScreen : SupportAppScreen() {
        override fun getFragment(): Fragment? {
            return HelloFragment.newInstance()
        }
    }

    class HomeFragmentScreen : SupportAppScreen() {
        override fun getFragment(): Fragment? {
            return HomeFragment.newInstance()
        }
    }

    class OrientationTabletFragmentScreen : SupportAppScreen() {
        override fun getFragment(): Fragment? {
            return OrientationTabletFragment.newInstance()
        }
    }


    class CoreFragmentScreen : SupportAppScreen() {
        override fun getFragment(): Fragment? {
            return CoreFragment.newInstance()
        }
    }

    class SelectSongFragmentScreen : SupportAppScreen() {
        override fun getFragment(): Fragment? {
            return SelectSongFragment.newInstance()
        }
    }

    class NewsFragmentScreen : SupportAppScreen() {
        override fun getFragment(): Fragment? {
            return NewsFragment.newInstance()
        }
    }

    class SelectedNewsFragmentScreen(private val news: News) : SupportAppScreen() {
        override fun getFragment(): Fragment? {
            return SelectedNewsFragment.newInstance(news)
        }
    }

    class CustomViewFragmentScreen() : SupportAppScreen() {
        override fun getFragment(): Fragment? {
            return CustomViewFragment.newInstance()
        }
    }

    class MapsFragmentScreen() : SupportAppScreen() {
        override fun getFragment(): Fragment? {
            return MapsFragment.newInstance()
        }
    }
}