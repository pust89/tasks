package com.pustovit.hellowword.fragments.news.presentation.adapter

class NewsItemClickListener<T>(val clickListener: (t: T) -> Unit) {

    fun onClick(t: T) {
        clickListener.invoke(t)
    }
}