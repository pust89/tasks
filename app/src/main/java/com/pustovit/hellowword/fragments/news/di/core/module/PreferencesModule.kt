package com.pustovit.hellowword.fragments.news.di.core.module

import android.content.Context
import com.pustovit.hellowword.fragments.news.data.pref.NewsPreferences
import com.pustovit.hellowword.fragments.news.data.pref.NewsPreferencesImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PreferencesModule {

    @Singleton
    @Provides
    fun provideNewsPreferences(context: Context): NewsPreferences {
        return NewsPreferencesImpl(context)
    }
}