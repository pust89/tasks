package com.pustovit.hellowword.fragments.recview

import android.os.Bundle
import android.view.*
import android.widget.Toast
import com.pustovit.hellowword.R
import com.pustovit.hellowword.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_figure_recycler_view.*

class FigureRecyclerViewFragment : BaseFragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        setHasOptionsMenu(true)

        return inflater.inflate(R.layout.fragment_figure_recycler_view, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter: FigureListAdapter =
            FigureListAdapter(
                FigureDiffUtilItemCallback(),
                ItemClickListener<Figure> { figure: Figure, position: Int ->
                    showAlertDialog(figure.description, position)
                })
        figure_recycler_view.adapter = adapter
        adapter.submitList(generateFigures())

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.recycler_fragment_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val sb = StringBuilder("")
        when (item.itemId) {
            R.id.menu_item_1 -> sb.append("First menu item")
            R.id.menu_item_2 -> sb.append("Second menu item")
            R.id.menu_item_3 -> sb.append("Third menu item")
        }
        Toast.makeText(activity, sb.toString(), Toast.LENGTH_SHORT).show()
        return super.onOptionsItemSelected(item)
    }

    override fun getTitleResId(): Int {
        return  R.string.recycler_view
    }

    companion object {
        fun newInstance() = FigureRecyclerViewFragment()
    }

    private fun showAlertDialog(description: String, position: Int) {
        activity?.supportFragmentManager?.let {
            val alertDialogFragment = MyAlertDialogFragment.newInstance(position, description)
            alertDialogFragment.show(it, "myAlertDialog")
        }
    }
}