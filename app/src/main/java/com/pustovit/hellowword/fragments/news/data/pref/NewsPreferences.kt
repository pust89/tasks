package com.pustovit.hellowword.fragments.news.data.pref

import java.util.*

interface NewsPreferences {

    fun saveLastQueryTime(time: Long)

    fun getLastQueryTime(): Long


}