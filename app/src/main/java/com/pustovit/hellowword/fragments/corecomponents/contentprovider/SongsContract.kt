package com.pustovit.hellowword.fragments.corecomponents.contentprovider

import android.content.ContentUris
import android.net.Uri
import com.pustovit.hellowword.fragments.corecomponents.contentprovider.SongProvider.Contracts.CONTENT_AUTHORITY_URI


class SongsContract private constructor()
{

    companion object {
        const val TABLE_NAME = "songs"

        /**
         * Uri to access the "songs_table" table.
         */
        val CONTENT_URI = Uri.withAppendedPath(CONTENT_AUTHORITY_URI, TABLE_NAME)

        val CONTENT_TYPE = "vnd.android.cursor.dir/vnd.$CONTENT_URI.$TABLE_NAME"
        val CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.$CONTENT_URI.$TABLE_NAME"

        fun buildSongUriWithId(songId: Long): Uri {
            return ContentUris.withAppendedId(CONTENT_URI, songId)
        }

        fun getSongIdFromUri(uri: Uri): Long {
            return ContentUris.parseId(uri)
        }

        const val COLUMN_ID = "_id"
        const val COLUMN_RESOURCE_ID = "resourceId"
        const val COLUMN_TITLE = "title"
        const val COLUMN_ARTIST = "artist"
        const val COLUMN_GENRE = "genre"

    }
}