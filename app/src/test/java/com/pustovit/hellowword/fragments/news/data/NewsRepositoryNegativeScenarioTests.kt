package com.pustovit.hellowword.fragments.news.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.never
import org.mockito.ArgumentMatchers.*

import com.nhaarman.mockitokotlin2.timeout
import com.nhaarman.mockitokotlin2.verify
import com.pustovit.hellowword.fragments.news.data.database.DatabaseNews
import com.pustovit.hellowword.fragments.news.data.datasource.NewsLocalDataSource
import com.pustovit.hellowword.fragments.news.data.datasource.NewsRemoteDataSource
import com.pustovit.hellowword.fragments.news.data.network.Article
import com.pustovit.hellowword.fragments.news.data.network.NewsResponsePage
import com.pustovit.hellowword.fragments.news.data.network.Source
import com.pustovit.hellowword.fragments.news.data.pref.NewsPreferences
import com.pustovit.hellowword.fragments.news.domain.News
import com.pustovit.hellowword.fragments.news.domain.NewsRepository
import com.pustovit.hellowword.fragments.news.domain.ResultOf
import io.reactivex.Observable
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.internal.matchers.InstanceOf
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class NewsRepositoryNegativeScenarioTests {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var localDataSource: NewsLocalDataSource

    @Mock
    private lateinit var remoteDataSource: NewsRemoteDataSource

    @Mock
    private lateinit var newsPreferences: NewsPreferences

    @Mock
    private lateinit var newsObserver: Observer<ResultOf<List<News>>>

    private var newsRepository: NewsRepository? = null


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @After
    fun tearDown() {
        newsRepository?.onCleared()
        newsRepository = null
    }


    /**
     * Тест кейс:
     * 1) Запрашиваем новости по запросу QUERY.
     * 2) В методе getNews определяется, что со времени предыдущего запрос прошло меньше 1 минуты,
     *    но запрос QUERY не содержиться в списке кэшированных запросов.
     * 3) Происходит запрос к api, который возращает NewsResponsePage с пустыи списком новостей.
     * 4) Сравниваем, что тестовый Observer получил ожидаемы  результат (ResultOf.Empty ).
     */
    @Test
    fun getNewsFromNetworkReturnEmptyList() {

        /* Ответ от api содержит пустой список новостей*/
        val newsResponsePage = NewsResponsePage(emptyList<Article>())

        /* Подменяем ответ от remoteDataSource.getNewsByNetwork(QUERY) */
        Mockito.`when`(remoteDataSource.getNewsByNetwork(QUERY))
            .thenReturn(Observable.just(newsResponsePage))

        /* Со времени предыдущего запрос прошло меньше 1 мин.*/
        Mockito.`when`(newsPreferences.getLastQueryTime()).thenReturn(Date().time)

        /* Возвращаем пустой список новостных тем, которые содержаться в кеше. */
        Mockito.`when`(localDataSource.getCachedQueries())
            .thenReturn(MutableLiveData<List<String>>(emptyList()))

        newsRepository =
            NewsRepositoryImpl(remoteDataSource, localDataSource, newsPreferences)

        /* Выполняем запрос и подписывам LiveData на тестовый Observer */
        newsRepository!!.getNews(QUERY).observeForever(newsObserver)

        verify(remoteDataSource, timeout(1000)).getNewsByNetwork(QUERY)

        /* Проверяем, что никакие новости не было сохранены в кэш.*/
        verify(localDataSource, never()).saveNews(any())

        verify(newsObserver, timeout(1000)).onChanged(expectedEmptyResult)
    }


    /**
     * Тест кейс:
     * 1) Запрашиваем новости по запросу QUERY.
     * 2) В методе getNews определяется, что со времени предыдущего запрос прошло меньше 1 минуты,
     *    но запрос QUERY не содержиться в списке кэшированных запросов.
     * 3) Происходит запрос к api, который возвращает Observable.error(testThrowable)
     * 4) Сравниваем, что тестовый Observer получил ожидаемы  результат (ResultOf.Failure(testThrowable)).
     */
    @Test
    fun getNewsFromNetworkCompletedWithError() {

        /* Подменяем ответ от remoteDataSource.getNewsByNetwork(QUERY) */
        Mockito.`when`(remoteDataSource.getNewsByNetwork(QUERY))
            .thenReturn(Observable.error(testThrowable))

        /* Со времени предыдущего запрос прошло меньше 1 мин.*/
        Mockito.`when`(newsPreferences.getLastQueryTime()).thenReturn(Date().time)

        /* Возвращаем пустой список новостных тем, которые содержаться в кеше. */
        Mockito.`when`(localDataSource.getCachedQueries())
            .thenReturn(MutableLiveData<List<String>>(emptyList()))

        newsRepository =
            NewsRepositoryImpl(remoteDataSource, localDataSource, newsPreferences)

        /* Выполняем запрос и подписывам LiveData на тестовый Observer */
        newsRepository!!.getNews(QUERY).observeForever(newsObserver)

        verify(remoteDataSource, timeout(1000)).getNewsByNetwork(QUERY)

        /* Проверяем, что никакие новости не было сохранены в кэш.*/
        verify(localDataSource, never()).saveNews(any())

        verify(newsObserver, timeout(1000)).onChanged(expectedFailureResult)
    }


    /**
     * Тест кейс:
     * 1) Запрашиваем новости по запросу QUERY.
     * 2) В методе getNews определяется, что со времени предыдущего запрос прошло меньше 1 минуты,
     *    но запрос QUERY не содержиться в списке кэшированных запросов.
     * 3) Происходит запрос к api, который возвращает Observable.empty().
     * 4) Сравниваем, что тестовый Observer получил ожидаемы  результат (ResultOf.Empty).
     */
    @Test
    fun getNewsFromNetworkCompletedWithEmpty() {

        /* Подменяем ответ от remoteDataSource.getNewsByNetwork(QUERY) */
        Mockito.`when`(remoteDataSource.getNewsByNetwork(QUERY))
            .thenReturn(Observable.empty())

        /* Со времени предыдущего запрос прошло меньше 1 мин.*/
        Mockito.`when`(newsPreferences.getLastQueryTime()).thenReturn(Date().time)

        /* Возвращаем пустой список новостных тем, которые содержаться в кеше. */
        Mockito.`when`(localDataSource.getCachedQueries())
            .thenReturn(MutableLiveData<List<String>>(emptyList()))

        newsRepository =
            NewsRepositoryImpl(remoteDataSource, localDataSource, newsPreferences)

        /* Выполняем запрос и подписывам LiveData на тестовый Observer */
        newsRepository!!.getNews(QUERY).observeForever(newsObserver)

        verify(remoteDataSource, timeout(1000)).getNewsByNetwork(QUERY)

        /* Проверяем, что никакие новости не было сохранены в кэш.*/
        verify(localDataSource, never()).saveNews(any())

        verify(newsObserver, timeout(1000)).onChanged(expectedEmptyResult)
    }



    /**
     * Тест кейс:
     * 1) Запрашиваем новости по запросу QUERY.
     * 2) В методе getNews определяется, что со времени предыдущего запрос прошло меньше 1 минуты,
     *    но запрос QUERY не содержиться в списке кэшированных запросов.
     * 3) Происходит запрос к api, который возращает NewsResponsePage с 4 новостями.
     *    Title кажой новости короче 20 символов.
     *      - внутри репозитория происходит фильтрация полученных новостей по длине тайтла
     *      - к тайтлу добавляется фраза "Filtered. "

     * 4) Сравниваем, что тестовый Observer получил ожидаемы  результат (ResultOf.Empty ).
     */
    @Test
    fun listFromNetworkWithShortTitleItems() {

        /* Ответ от api содержит пустой список новостей*/
        val newsResponsePage = NewsResponsePage(testArticles)

        /* Подменяем ответ от remoteDataSource.getNewsByNetwork(QUERY) */
        Mockito.`when`(remoteDataSource.getNewsByNetwork(QUERY))
            .thenReturn(Observable.just(newsResponsePage))

        /* Со времени предыдущего запрос прошло меньше 1 мин.*/
        Mockito.`when`(newsPreferences.getLastQueryTime()).thenReturn(Date().time)

        /* Возвращаем пустой список новостных тем, которые содержаться в кеше. */
        Mockito.`when`(localDataSource.getCachedQueries())
            .thenReturn(MutableLiveData<List<String>>(emptyList()))

        newsRepository =
            NewsRepositoryImpl(remoteDataSource, localDataSource, newsPreferences)

        /* Выполняем запрос и подписывам LiveData на тестовый Observer */
        newsRepository!!.getNews(QUERY).observeForever(newsObserver)

        verify(remoteDataSource, timeout(1000)).getNewsByNetwork(QUERY)

        /* Проверяем, что никакие новости не было сохранены в кэш.*/
        verify(localDataSource, never()).saveNews(any())

        verify(newsObserver, timeout(1000)).onChanged(expectedEmptyResult)
    }


    companion object {

        private const val QUERY = "software"
        val testArticles = listOf<Article>(
            Article(
                "author1", "content1", "description1",
                "2020-10-26", Source("sourceName1"), "title1", "url1",
                "urlToImage1"
            ),
            Article(
                "author2", "content2", "description2",
                "2020-10-26", Source("sourceName2"), "title2", "url2",
                "urlToImage2"
            ),
            Article(
                "author3", "content3", "description3",
                "2020-10-26", Source("sourceName3"), "title3", "url3",
                "urlToImage3"
            ),
            Article(
                "author4", "content4", "description4",
                "2020-10-26", Source("sourceName4"), "title4", "url4",
                "urlToImage4"
            )
        )

    }

    private val expectedEmptyResult = ResultOf.Empty

    private val throwableMessage = "test throwable 123"
    private val testThrowable = Throwable(throwableMessage)
    private val expectedFailureResult = ResultOf.Failure(testThrowable)
}