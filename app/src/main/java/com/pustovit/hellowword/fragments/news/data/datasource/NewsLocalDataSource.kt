package com.pustovit.hellowword.fragments.news.data.datasource

import androidx.lifecycle.LiveData
import com.pustovit.hellowword.fragments.news.data.database.DatabaseNews
import com.pustovit.hellowword.fragments.news.domain.News
import com.pustovit.hellowword.fragments.news.domain.ResultOf

interface NewsLocalDataSource {

    fun saveNews(databaseNews:List<DatabaseNews>)

    fun getNews(query:String):List<DatabaseNews>

    fun getCachedQueries():LiveData<List<String>>

    fun clearCache()

}