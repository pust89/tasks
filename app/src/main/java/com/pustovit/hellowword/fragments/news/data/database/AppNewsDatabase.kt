package com.pustovit.hellowword.fragments.news.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = [DatabaseNews::class],
    version = 1,
    exportSchema = false
)
abstract class AppNewsDatabase() : RoomDatabase() {

    abstract fun databaseNewsDao(): DatabaseNewsDao

    companion object {
        private const val DB_NAME = "news_database"

        @Volatile
        private var INSTANCE: AppNewsDatabase? = null

        fun getInstance(context: Context): AppNewsDatabase {
            synchronized(AppNewsDatabase::class.java) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppNewsDatabase::class.java,
                        DB_NAME
                    )
                        .fallbackToDestructiveMigration()
                        .build()

                    INSTANCE = instance
                }
                return instance
            }
        }
    }

}