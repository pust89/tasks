package com.pustovit.hellowword.fragments.news.presentation

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.pustovit.hellowword.App
import com.pustovit.hellowword.MainActivity
import com.pustovit.hellowword.R
import com.pustovit.hellowword.databinding.FragmentNewsBinding
import com.pustovit.hellowword.fragments.news.di.news.NewsComponent
import com.pustovit.hellowword.fragments.news.domain.News
import com.pustovit.hellowword.fragments.news.presentation.adapter.NewsItemClickListener
import com.pustovit.hellowword.fragments.news.presentation.adapter.NewsListAdapter
import com.pustovit.hellowword.fragments.news.presentation.dialogs.DownloadDialogFragment
import com.pustovit.hellowword.fragments.news.presentation.dialogs.ErrorDialogFragment
import com.pustovit.hellowword.navigation.Screens
import kotlinx.android.synthetic.main.fragment_news.*
import timber.log.Timber
import javax.inject.Inject

class NewsFragment : Fragment() {


    private lateinit var viewModel: NewsFragmentViewModel
    private lateinit var binding: FragmentNewsBinding

    private val newsComponent: NewsComponent by lazy {
        (activity?.application as App).appComponent.getNewsComponent().create()
    }

    @Inject
    lateinit var viewModelFactory: NewsFragmentViewModelFactory

    override fun onAttach(context: Context) {
        super.onAttach(context)
        newsComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this, viewModelFactory).get(NewsFragmentViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_news, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel

        binding.topicsSpinner.apply {
            adapter = topicsAdapter
            onItemSelectedListener = onTopicsSelectedListener
        }

        newsRecView.adapter = newsAdapter

        binding.swipeRefresh.setOnRefreshListener {
            viewModel.refreshQuery()
        }

        observeLiveData()
    }

    private fun observeLiveData() {
        viewModel.titleLiveData.observe(viewLifecycleOwner) {
            activity?.title = it
        }

        viewModel.downloadingDialogLiveData.observe(viewLifecycleOwner) {
            if (it) {
                displayDownloadDialog()
            } else {
                hideDownloadDialog()
            }
        }

        viewModel.errorDialogLiveData.observe(viewLifecycleOwner) {
            if (it != null) {
                showErrorDialog(it)
            }
        }

        viewModel.swipeRefreshLiveData.observe(viewLifecycleOwner) {
            binding.swipeRefresh.isRefreshing = it

        }

    }

    private val newsAdapter: NewsListAdapter by lazy {
        NewsListAdapter(NewsItemClickListener<News> {
            (requireActivity() as MainActivity)
                .router
                .navigateTo(Screens.SelectedNewsFragmentScreen(it))
        })

    }

    private val topicsAdapter: ArrayAdapter<String> by lazy {
        ArrayAdapter<String>(
            requireContext(),
            android.R.layout.simple_list_item_1,
            ArrayList<String>()
        )
    }

    private val onTopicsSelectedListener: AdapterView.OnItemSelectedListener by lazy {
        object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                viewModel.onSelectTopic(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
    }

    private fun showErrorDialog(message: String) {
        activity?.supportFragmentManager?.let {
            val fragment = it.findFragmentByTag(ErrorDialogFragment.TAG)
            if (fragment == null) {
                ErrorDialogFragment.newInstance(message).apply {
                    show(it, ErrorDialogFragment.TAG)
                }
            }
        }
    }

    private fun displayDownloadDialog() {
        activity?.supportFragmentManager?.let {
            val downloadDialog = it.findFragmentByTag(DownloadDialogFragment.TAG)
            if (downloadDialog == null) {
                DownloadDialogFragment.newInstance().apply {
                    isCancelable = false
                    show(it, DownloadDialogFragment.TAG)
                }
            }
        }
    }

    private fun hideDownloadDialog() {
        activity?.supportFragmentManager?.let { fragmentManager ->
            Handler(Looper.getMainLooper()).postDelayed({
                // Костыль! Был баг, когда лайвдата с командой "закрыть диалог загрузки", отправлялась раньше
                // чем сам диалог загрузки успевал прорисовываться, и метод hideDownloadDialog не отрабатывал
                // должным образом, т.к. это чисто проблемы скорости отрисовки, то решил запихнуть его во фрагмент.
                run {
                    val fragment = fragmentManager.findFragmentByTag(DownloadDialogFragment.TAG)
                    fragment?.let {
                        (it as DialogFragment).dismiss()
                    }
                }
            }, 500)
        }
    }

    companion object {
        fun newInstance() = NewsFragment()
    }
}
