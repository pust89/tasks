package com.pustovit.hellowword.fragments.orientationtablet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pustovit.hellowword.R
import com.pustovit.hellowword.common.BaseFragment


class OrientationTabletFragment : BaseFragment() {
    override fun getTitleResId(): Int {
        return R.string.orientation_tablet
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_orientation_tablet, container, false)
    }

    companion object {
        fun newInstance() = OrientationTabletFragment()
    }
}