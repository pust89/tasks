package com.pustovit.hellowword.fragments.recview

class ItemClickListener<T>(private val itemClickListener: (t: T, position: Int) -> Unit) {

    fun onClick(t: T, position: Int) {
        itemClickListener.invoke(t, position)
    }
}
