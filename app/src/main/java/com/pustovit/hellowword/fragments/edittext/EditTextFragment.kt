package com.pustovit.hellowword.fragments.edittext

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.pustovit.hellowword.R
import com.pustovit.hellowword.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_edit_text.*

class EditTextFragment : BaseFragment() {

    private val textWatcher: TextWatcher = object : TextWatcher {

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable?) {
            if (s.isNullOrEmpty()) return
            Toast.makeText(context, s.toString(), Toast.LENGTH_SHORT).show()
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edit_text, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editTextView.addTextChangedListener(textWatcher)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        editTextView.removeTextChangedListener(textWatcher)
    }

    override fun getTitleResId(): Int {
        return R.string.edittext
    }


    companion object {
        fun newInstance() =
            EditTextFragment()
    }
}