package com.pustovit.hellowword.fragments.news.di.core

import android.content.Context
import com.pustovit.hellowword.fragments.news.di.core.module.*
import com.pustovit.hellowword.fragments.news.di.news.NewsComponent
import com.pustovit.hellowword.fragments.news.di.news.NewsModule
import com.pustovit.hellowword.fragments.news.presentation.NewsFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [SubcomponentsModule::class,
        NetworkModule::class,
        DatabaseModule::class,
        RepositoryModule::class,
        LocalDataModule::class,
        RemoteDataModule::class,
        PreferencesModule::class]
)
interface AppComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): AppComponent
    }

    // Types that can be retrieved from the graph
    fun getNewsComponent(): NewsComponent.Factory
}