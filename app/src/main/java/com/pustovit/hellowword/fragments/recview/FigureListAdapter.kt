package com.pustovit.hellowword.fragments.recview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.pustovit.hellowword.R
import kotlinx.android.synthetic.main.item_figure.view.*

class FigureListAdapter(
    figureDiffUtilItemCallback: FigureDiffUtilItemCallback,
    private val figureItemClickListener: ItemClickListener<Figure>
) : ListAdapter<Figure, FigureVh>(figureDiffUtilItemCallback) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FigureVh {
        return FigureVh(parent.inflate(R.layout.item_figure))
    }

    override fun onBindViewHolder(holder: FigureVh, position: Int) {
        holder.bind(getItem(position), position, figureItemClickListener)
    }

}

class FigureVh(private val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(figure: Figure, position: Int, figureItemClickListener: ItemClickListener<Figure>) {
        view.apply {
            item_image_view.setImageResource(figure.resourceId)
            item_tv_description.text = figure.description
            setOnClickListener {
                figureItemClickListener.onClick(figure, position)
            }
        }
    }

}


fun ViewGroup.inflate(layoutResId: Int, root: ViewGroup? = this, attachToRoot: Boolean = false): View =
    LayoutInflater.from(context).inflate(layoutResId, root, attachToRoot)



