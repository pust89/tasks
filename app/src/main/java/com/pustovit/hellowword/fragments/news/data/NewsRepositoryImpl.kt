package com.pustovit.hellowword.fragments.news.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.pustovit.hellowword.fragments.news.data.database.asDomainModel
import com.pustovit.hellowword.fragments.news.data.datasource.NewsLocalDataSource
import com.pustovit.hellowword.fragments.news.data.datasource.NewsRemoteDataSource
import com.pustovit.hellowword.fragments.news.data.network.asDatabaseModel
import com.pustovit.hellowword.fragments.news.data.network.asDomainModel
import com.pustovit.hellowword.fragments.news.data.pref.NewsPreferences
import com.pustovit.hellowword.fragments.news.domain.News
import com.pustovit.hellowword.fragments.news.domain.NewsRepository
import com.pustovit.hellowword.fragments.news.domain.ResultOf
import timber.log.Timber
import java.util.*
import androidx.lifecycle.Observer
import com.pustovit.hellowword.fragments.news.data.database.DatabaseNews
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Function
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executors
import kotlin.collections.ArrayList


class NewsRepositoryImpl(
    private val remoteDataSource: NewsRemoteDataSource,
    private val localDataSource: NewsLocalDataSource,
    private var newsPreferences: NewsPreferences
) : NewsRepository {


    // Благодаря лайвдате из руум, всегда получаем свежую информацию о кешированных
    // запросах (cachedQueries = [football, covid, cars])
    private val cachedQueries = ArrayList<String>()

    private val cachedQueriesObserver = Observer<List<String>> {
        cachedQueries.clear()
        cachedQueries.addAll(it)
        Timber.d("cachedQueries = ${cachedQueries.toString()}")
    }

    private fun setCachedQueriesLiveData() {
        localDataSource.getCachedQueries().observeForever(cachedQueriesObserver)
    }

    init {
        setCachedQueriesLiveData()
    }


    private val newsLiveData = MutableLiveData<ResultOf<List<News>>>()

    private var lastRequestTime: Long = newsPreferences.getLastQueryTime()

    private fun timeCheck(): Boolean {
        val currentTime = Date().time
        return if (currentTime - lastRequestTime < 60000) {
            true

        } else {
            lastRequestTime = currentTime
            false
        }
    }

    override fun getNews(query: String): LiveData<ResultOf<List<News>>> {

        if (timeCheck()) {
            if (cachedQueries.contains(query)) {
                Executors.newSingleThreadExecutor().execute {
                    newsLiveData.postValue(ResultOf.Success(getNewsByFromCache(query)))
                }
            } else {
                getNewsByNetwork(query)
            }
        } else {
            Executors.newSingleThreadExecutor().execute {
                localDataSource.clearCache()
            }
            getNewsByNetwork(query)
        }

        return newsLiveData
    }


    private val compositeDisposable = CompositeDisposable()

    private fun getNewsByNetwork(query: String) {
        Timber.d("getNewsByNetwork called")
        val databaseNewsList = ArrayList<DatabaseNews>()
        compositeDisposable.add(remoteDataSource.getNewsByNetwork(query)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .flatMapIterable {
                // достаем из NewsResponsePage список новостей(список data transfer object-ов)
                // мэпим этот список в список database object-ов
                // и превращаем каждый элемент списка в Observable
                it.articles.asDatabaseModel(query)
            }
            .filter {
                it.title.length > 20
            }
            .map { databaseNews ->
                databaseNews.title = "Filtered. ${databaseNews.title}"
                databaseNews
            }.subscribe(
                { databaseNewsList.add(it) },
                { newsLiveData.postValue(ResultOf.Failure(it)) },
                {
                    if (databaseNewsList.isNotEmpty()) {
                        // Кэшируем данные здесь
                        // т.к. обозреваемся в Schedulers.io(), то не надо создавать
                        // отдельного треда для работы с localDataSource
                        localDataSource.saveNews(databaseNewsList)
                        newsLiveData.postValue(ResultOf.Success(databaseNewsList.asDomainModel()))
                    } else {
                        newsLiveData.postValue(ResultOf.Empty)
                    }
                })
        )

    }

    private fun getNewsByFromCache(query: String): List<News> {
        Timber.d("getNewsByFromCache called")
        return localDataSource.getNews(query).asDomainModel()
    }

    override fun onCleared() {
        newsPreferences.saveLastQueryTime(lastRequestTime)
        localDataSource.getCachedQueries().removeObserver(cachedQueriesObserver)
        compositeDisposable.clear()
    }

}