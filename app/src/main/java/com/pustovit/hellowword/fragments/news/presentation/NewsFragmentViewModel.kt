package com.pustovit.hellowword.fragments.news.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.pustovit.hellowword.fragments.news.domain.News
import com.pustovit.hellowword.fragments.news.domain.NewsRepository
import com.pustovit.hellowword.fragments.news.domain.ResultOf
import timber.log.Timber

class NewsFragmentViewModel(private val repository: NewsRepository) :
    ViewModel() {

    companion object {
        private val TOPICS: List<String> =
            listOf("software", "cars", "android", "football", "covid")
    }

    init {
        Timber.d("NewsFragmentViewModel created")
    }

    val topics = TOPICS

    private var currentTopic = ""
    private var flagIsFailure = false

    private var viewModelObserver = Observer<ResultOf<List<News>>> {
        when (it) {
            is ResultOf.Success -> {
                hideCompositeDownloading()
                innerNewsLiveData.value = it.value

                if (flagIsFailure) {
                    flagIsFailure = false
                    innerErrorDialogLiveData.value = null
                }
            }

            is ResultOf.Failure -> {
                hideCompositeDownloading()
                flagIsFailure = true
                innerErrorDialogLiveData.value = it.throwable?.message
            }

            is ResultOf.Empty -> {
                hideCompositeDownloading()
            }
        }
    }


    private val innerNewsLiveData: MutableLiveData<List<News>> = MutableLiveData()
    val newsLiveData: LiveData<List<News>>
        get() = innerNewsLiveData


    private val innerSelectedTopicPositionLiveData: MutableLiveData<Int> = MutableLiveData(0)
    val selectedTopicPositionLiveData: LiveData<Int>
        get() = innerSelectedTopicPositionLiveData

    private val innerTitleLiveData: MutableLiveData<String> = MutableLiveData("software")
    val titleLiveData: LiveData<String>
        get() = innerTitleLiveData

    private var flagIsDownloading = false

    fun onSelectTopic(position: Int) {
        val desiredTopic = topics[position]
        if (desiredTopic != currentTopic) {

            flagIsDownloading = true

            innerSelectedTopicPositionLiveData.value = position
            innerTitleLiveData.value = desiredTopic

            currentTopic = desiredTopic
            executeQuery(desiredTopic)
        }

    }

    private var flagIsRefreshing = false

    fun refreshQuery() {
        flagIsRefreshing = true
        executeQuery(currentTopic)
    }

    private fun executeQuery(query: String) {
        displayCompositeDownloading()
        repository.getNews(query).observeForever(viewModelObserver)
    }


    /*Work with downloading dialog*/
    private val innerDownloadingDialogLiveData = MutableLiveData<Boolean>()
    val downloadingDialogLiveData: LiveData<Boolean>
        get() = innerDownloadingDialogLiveData


    /*Work with error dialog*/
    private val innerErrorDialogLiveData = MutableLiveData<String?>()
    val errorDialogLiveData: LiveData<String?>
        get() = innerErrorDialogLiveData


    /*Work with swipe refresh*/
    private val innerSwipeRefreshLiveData = MutableLiveData<Boolean>()
    val swipeRefreshLiveData: LiveData<Boolean>
        get() = innerSwipeRefreshLiveData


    private fun displayCompositeDownloading() {
        if (flagIsDownloading) {
            innerDownloadingDialogLiveData.value = true
        }

        if (flagIsRefreshing) {
            innerSwipeRefreshLiveData.value = true
        }
    }


    private fun hideCompositeDownloading() {
        if (flagIsDownloading) {
            innerDownloadingDialogLiveData.value = false
            flagIsDownloading = false
        }

        if (flagIsRefreshing) {
            innerSwipeRefreshLiveData.value = false
            flagIsRefreshing = false
        }
    }

    override fun onCleared() {
        repository.onCleared()
        super.onCleared()
    }
}