package com.pustovit.hellowword.fragments.hello

import android.database.Cursor
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pustovit.hellowword.R
import com.pustovit.hellowword.common.BaseFragment
import com.pustovit.hellowword.fragments.corecomponents.contentprovider.SongsContract


class HelloFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_hello, container, false)
    }

    override fun getTitleResId(): Int {
        return R.string.helloAndersen
    }


    companion object {
        fun newInstance() =
            HelloFragment()
    }
}