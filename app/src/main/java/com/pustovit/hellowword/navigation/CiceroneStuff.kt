package com.pustovit.hellowword.navigation

import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router


interface NavigatorHolderProvider {
    fun getNavigatorHolder(): NavigatorHolder
}

interface RouterProvider {
    fun getRouter(): Router
}