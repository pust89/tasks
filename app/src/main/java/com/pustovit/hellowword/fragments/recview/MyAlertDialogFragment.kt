package com.pustovit.hellowword.fragments.recview

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.pustovit.hellowword.App.Companion.LOG_TAG
import com.pustovit.hellowword.R


class MyAlertDialogFragment : DialogFragment() {

    private val mPosition by lazy {
        arguments?.let {
            it.getInt(ARG_PARAM_POSITION)}
    }

    private val mDescription by lazy {
        arguments?.let {
            it.getString(ARG_PARAM_DESCRIPTION)}
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it)
            builder.setMessage(buildMessage(mPosition, mDescription))
                .setPositiveButton(R.string.ok,
                    DialogInterface.OnClickListener { dialog, id ->
                        Log.i(LOG_TAG, "onCreateDialog: positive btn pressed")
                    })
                .setNegativeButton(R.string.cancel,
                    DialogInterface.OnClickListener { dialog, id ->
                        Log.i(LOG_TAG, "onCreateDialog: negative btn pressed")
                    })
            // Create the AlertDialog object and return it
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    private fun buildMessage(position: Int?, description: String?): String {
        var message: StringBuilder = StringBuilder("")
        position?.let {
            message.append(it.toString()).append(" - ")
            when (it) {
                0 -> message.append("Нулевой")
                1 -> message.append("Первый")
                2 -> message.append("Второй")
                3 -> message.append("Третий")
                4 -> message.append("Четвертый")
                5 -> message.append("Пятый")
                6 -> message.append("Шестой")
                7 -> message.append("Седьмой")
                8 -> message.append("Восьмой")
                9 -> message.append("Девятый")
                else -> message.append("")
            }
            message.append("\n").append(description)
        }
        return message.toString()
    }

    companion object {
        private const val ARG_PARAM_POSITION = "position"
        private const val ARG_PARAM_DESCRIPTION = "description"

        fun newInstance(position: Int, description: String) =
            MyAlertDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_PARAM_POSITION, position)
                    putString(ARG_PARAM_DESCRIPTION, description)
                }
            }
    }

}