package com.pustovit.hellowword.fragments.news.presentation

import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.pustovit.hellowword.fragments.news.domain.News
import com.pustovit.hellowword.fragments.news.presentation.adapter.NewsListAdapter
import timber.log.Timber


@BindingAdapter(value = ["newsList"])
fun bindRecyclerViewNews(recyclerView: RecyclerView, data: List<News>?) {
    val adapter = recyclerView.adapter as NewsListAdapter
        adapter.submitList(data)
    }

@Suppress("UNCHECKED_CAST")
@BindingAdapter(value = ["topicsList"])
fun onDownloadTopics(spinner: Spinner,topics: List<String>) {
    val topicsAdapter = spinner.adapter as ArrayAdapter<String>
    if (topicsAdapter.isEmpty) {
        topicsAdapter.addAll(topics)
    }
}
