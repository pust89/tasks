package com.pustovit.hellowword.fragments.corecomponents

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.pustovit.hellowword.R
import com.pustovit.hellowword.fragments.recview.ItemClickListener
import kotlinx.android.synthetic.main.item_song.view.*

class SongListAdapter(
    songDiffUtilItemCallback: SongDiffUtilItemCallback,
    private val songItemClickListener: ItemClickListener<Song>
) : ListAdapter<Song, SongVh>(songDiffUtilItemCallback) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongVh {
        return SongVh(parent.inflate(R.layout.item_song))
    }

    override fun onBindViewHolder(holder: SongVh, position: Int) {
        holder.bind(getItem(position), position, songItemClickListener)
    }

}

class SongVh(private val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(song: Song, position: Int, songItemClickListener: ItemClickListener<Song>) {
        view.apply {
            item_tv_genre.text = song.genre
            item_tv_artist.text = song.artist
            item_tv_title.text = song.title
            setOnClickListener {
                songItemClickListener.onClick(song, position)
            }
        }
    }

}


fun ViewGroup.inflate(layoutResId: Int, root: ViewGroup? = this, attachToRoot: Boolean = false): View =
    LayoutInflater.from(context).inflate(layoutResId, root, attachToRoot)



