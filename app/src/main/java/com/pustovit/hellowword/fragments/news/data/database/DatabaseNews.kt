package com.pustovit.hellowword.fragments.news.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.pustovit.hellowword.fragments.news.domain.News

@Entity(tableName = "news_table")
data class DatabaseNews(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val query:String,
    val author: String,
    val content: String,
    val description: String,
    val publishedAt: String,
    val sourceName: String,
    var title: String,
    val url: String,
    val urlToImage: String?
)

fun List<DatabaseNews>.asDomainModel(): List<News> {
    return this.map {
        News(
            author = it.author,
            content = it.content,
            description = it.description,
            publishedAt = it.publishedAt,
            sourceName = it.sourceName,
            title = it.title,
            url = it.url,
            urlToImage = it.urlToImage
        )
    }
}