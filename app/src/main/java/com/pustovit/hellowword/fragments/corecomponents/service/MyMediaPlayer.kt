package com.pustovit.hellowword.fragments.corecomponents.service

import android.content.Context
import android.media.MediaPlayer
import com.pustovit.hellowword.common.NotificationModule
import com.pustovit.hellowword.fragments.corecomponents.Song

class MyMediaPlayer {

    var currentSong: Song = Song(_id = -99, "stub", 0, "stub", "stub", 0)

    private var mediaPlayer: MediaPlayer? = null
    private var onPause = false

    fun play(context: Context, song: Song) {
        if(currentSong._id != song._id){
            NotificationModule.postNotificationSong(context, song)
        }
        if (onPause) {
            mediaPlayer?.apply {
                this.start()
                onPause = false
            }
            return
        }

        if (!song.equals(currentSong)) {
            stop()
            currentSong = song
            mediaPlayer = MediaPlayer.create(context, currentSong.resourceId).apply {
                setOnCompletionListener(onCompletionListener)
                this.seekTo(currentSong.currentPosition)
                start()
            }
        } else {
            mediaPlayer?.let {
                if (!it.isPlaying) it.start()
            }
        }
    }

    fun pause() {
        mediaPlayer?.apply {
            if (this.isPlaying) {
                pause()
                currentSong.currentPosition = this.currentPosition
            }
            onPause = true
        }
    }

    fun stop() {
        mediaPlayer?.apply {
            currentSong.currentPosition = this.currentPosition
            stop()
        }
    }

    private val onCompletionListener: MediaPlayer.OnCompletionListener =
        object : MediaPlayer.OnCompletionListener {
            override fun onCompletion(mp: MediaPlayer?) {
                currentSong.currentPosition = 0
            }
        }

    fun clear() {
        mediaPlayer?.apply {
            release()
        }
    }
}