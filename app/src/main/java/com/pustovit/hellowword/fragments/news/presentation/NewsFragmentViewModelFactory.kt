package com.pustovit.hellowword.fragments.news.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pustovit.hellowword.fragments.news.domain.NewsRepository
import java.lang.IllegalArgumentException

@Suppress("UNCHECKED_CAST")
class NewsFragmentViewModelFactory(private val repository: NewsRepository):ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(NewsFragmentViewModel::class.java)){
            return NewsFragmentViewModel(repository) as T
        } else{
            throw IllegalArgumentException("Unknown modelClass ${modelClass.simpleName}")
        }
    }
}