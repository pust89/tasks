package com.pustovit.hellowword.fragments.news.data.network

import retrofit2.Response

internal const val UNKNOWN_CODE = -1

sealed class ApiResponse<T> {
    companion object {
        fun <T> create(response: Response<T>): ApiResponse<T> {
            return if (response.isSuccessful) {
                val body = response.body()
                if (body == null || response.code() == 204) {
                    EmptyResponse()
                } else {
                    Success(body)
                }
            } else {
                Error(response.code(), response.errorBody()?.string()?:response.message())
            }
        }

        fun <T> create(errorCode: Int, error: Throwable): Error<T> {
            return Error(errorCode, error.message ?: "Unknown Error!")
        }
    }
    class EmptyResponse<T> : ApiResponse<T>()
    data class Error<T>(val errorCode: Int, val errorMessage: String): ApiResponse<T>()
    data class Success<T>(val body: T): ApiResponse<T>()
}
