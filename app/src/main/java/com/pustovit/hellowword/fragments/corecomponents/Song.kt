package com.pustovit.hellowword.fragments.corecomponents

import android.os.Parcelable
import com.pustovit.hellowword.fragments.corecomponents.contentprovider.database.entity.DatabaseSong
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Song(
    val _id: Long,
    val title: String="",
    val resourceId: Int=0,
    val artist: String="",
    val genre: String="",
    var currentPosition: Int = 0
) : Parcelable