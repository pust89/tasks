package com.pustovit.hellowword.navigation

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import com.pustovit.hellowword.R
import com.pustovit.hellowword.fragments.news.presentation.NewsFragment
import com.pustovit.hellowword.fragments.news.presentation.SelectedNewsFragment
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command

class MyNavigator(activity: FragmentActivity, containerId: Int) :
    SupportAppNavigator(activity, containerId) {

    override fun setupFragmentTransaction(
        command: Command,
        currentFragment: Fragment?,
        nextFragment: Fragment?,
        fragmentTransaction: FragmentTransaction
    ) {
        if (currentFragment is NewsFragment && nextFragment is SelectedNewsFragment) {
            fragmentTransaction.setCustomAnimations(
                R.anim.frag_in,
                R.anim.fade_out,
                0,
                R.anim.frag_out
            )
        }

        super.setupFragmentTransaction(
            command,
            currentFragment,
            nextFragment,
            fragmentTransaction
        )
    }
}
