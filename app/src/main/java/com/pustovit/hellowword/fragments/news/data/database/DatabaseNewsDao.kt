package com.pustovit.hellowword.fragments.news.data.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface DatabaseNewsDao {

    @Query("SELECT * FROM news_table WHERE `query` =:query;")
    fun getNewsByQuery(query: String): List<DatabaseNews>

    @Query("SELECT DISTINCT `query` FROM news_table;")
    fun getCachedQuery(): LiveData<List<String>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNews(news: List<DatabaseNews>)

    @Query("DELETE FROM news_table;")
    fun clearDatabase()
}