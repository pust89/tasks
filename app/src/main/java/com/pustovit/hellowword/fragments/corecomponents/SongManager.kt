package com.pustovit.hellowword.fragments.corecomponents

import android.content.ContentResolver
import com.pustovit.hellowword.fragments.corecomponents.contentprovider.SongsContract
import timber.log.Timber
import java.util.concurrent.TimeUnit


class SongManager private constructor(contentResolver: ContentResolver) {

    private var _songs: List<Song>
    var genres: List<String>
    var artists: List<String>

    val isActive: Boolean

    lateinit var songs: MutableList<Song>

    init {

        var songsCursor = contentResolver.query(
            SongsContract.CONTENT_URI, null, null, null, null
        )

        if (songsCursor != null) {
            _songs = songsCursor.asListSong()

            genres = feelSubList(_songs, SongsContract.COLUMN_GENRE)
            artists = feelSubList(_songs, SongsContract.COLUMN_ARTIST)

            songs = ArrayList(_songs)

            isActive = true
            songsCursor.close()

            for(i in _songs.indices) {
                Timber.d(_songs[i].toString())
            }

        } else {
            _songs = emptyList()
            genres = emptyList()
            artists = emptyList()
            isActive = false
        }

    }

    fun sortedByGenre(genre: String): List<Song> {
        if (genre.equals(SPINNER_TITLE_GENRE)) {
            songs.clear()
            songs = ArrayList(_songs)
            return songs
        }
        val sortedList: MutableList<Song> = ArrayList()
        for (i in 0.._songs.lastIndex) {
            if (_songs[i].genre.equals(genre)) {
                sortedList.add(_songs[i])
            }
        }
        songs.clear()
        songs = sortedList
        return songs
    }

    fun sortedByArtist(artist: String): List<Song> {
        if (artist.equals(SPINNER_TITLE_ARTIST)) {
            songs.clear()
            songs = ArrayList(_songs)
            return songs
        }
        val sortedList: MutableList<Song> = ArrayList()
        for (i in 0.._songs.lastIndex) {
            if (_songs[i].artist.equals(artist)) {
                sortedList.add(_songs[i])
            }
        }
        songs.clear()
        songs = sortedList
        return songs
    }

    private fun feelSubList(songs: List<Song>, column: String): List<String> {
        val set: MutableSet<String> = HashSet()

        if (column.equals(SongsContract.COLUMN_ARTIST)) {
            songs.forEach {
                set.add(it.artist)
            }
            return set.toMutableList().apply {
                add(0, "Исполнитель")
            }
        }

        if (column.equals(SongsContract.COLUMN_GENRE)) {
            songs.forEach {
                set.add(it.genre)
            }
            return set.toMutableList().apply {
                add(0, "Жанр")
            }
        }
        throw IllegalArgumentException("Unknown arg$column")
    }

    companion object {
        private const val SPINNER_TITLE_GENRE = "Жанр"
        private const val SPINNER_TITLE_ARTIST = "Исполнитель"

        private var mSongManager: SongManager? = null
        fun getInstance(contentResolver: ContentResolver): SongManager {
            if (mSongManager == null) {
                mSongManager = SongManager(contentResolver)
            }
            return mSongManager as SongManager
        }

        /*Вызываем в CoreFragment плеера, в onDestroy*/
        fun clear() {
            mSongManager?.apply {
                mSongManager = null
            }
        }
    }
}




