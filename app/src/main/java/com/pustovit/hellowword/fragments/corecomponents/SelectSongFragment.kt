package com.pustovit.hellowword.fragments.corecomponents

import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.loader.app.LoaderManager
import androidx.loader.content.AsyncTaskLoader
import androidx.loader.content.Loader
import com.pustovit.hellowword.R
import com.pustovit.hellowword.common.BaseFragment
import com.pustovit.hellowword.fragments.corecomponents.CoreFragment.Companion.BROADCAST_ACTION
import com.pustovit.hellowword.fragments.corecomponents.CoreFragment.Companion.BROADCAST_ACTION_SELECT_SONG

import com.pustovit.hellowword.fragments.corecomponents.service.PlayerService
import com.pustovit.hellowword.fragments.corecomponents.service.PlayerService.Companion.EXTRA_SONG
import com.pustovit.hellowword.fragments.recview.ItemClickListener
import com.pustovit.hellowword.navigation.Screens
import kotlinx.android.synthetic.main.fragment_select_song.*
import timber.log.Timber
import java.lang.Exception


class SelectSongFragment : BaseFragment(), LoaderManager.LoaderCallbacks<SongManager> {

    companion object {
        fun newInstance() = SelectSongFragment()
        private const val LOADER_ID = 90
        private const val SPINNER_POSITION_GENRE = "spinnerPositionGenre"
        private const val SPINNER_POSITION_ARTIST = "spinnerPositionArtist"
        const val EXTRA_SONG_FROM_LIST = "songFromList"
    }

    private lateinit var mSongManager: SongManager
    private lateinit var songAdapter: SongListAdapter

    private var spinnerPositionGenre: Int = 0
    private var spinnerPositionArtist: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        LoaderManager.getInstance(this).initLoader(LOADER_ID, null, this)

        savedInstanceState?.apply {
            spinnerPositionGenre = this.getInt(SPINNER_POSITION_GENRE)
            spinnerPositionArtist = this.getInt(SPINNER_POSITION_ARTIST)

        }
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.apply {
            putInt(SPINNER_POSITION_GENRE, spinnerPositionGenre)
            putInt(SPINNER_POSITION_ARTIST, spinnerPositionArtist)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_select_song, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
    }

    override fun getTitleResId(): Int {
        return R.string.title_core_fragment
    }

    private fun setupSpinners(songManager: SongManager) {

        genreSpinner.adapter = ArrayAdapter<String>(
            requireContext(),
            android.R.layout.simple_list_item_1,
            songManager.genres
        )

        genreSpinner.setSelection(spinnerPositionGenre)
        genreSpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                adapterView?.apply {
                    if (position != spinnerPositionGenre) {
                        spinnerPositionGenre = position
                        val selectedItem = adapterView.getItemAtPosition(position).toString()
                        songAdapter.submitList(songManager.sortedByGenre(selectedItem))
                        songAdapter.notifyDataSetChanged()
                    }
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                //Nothing
            }
        }


        artistSpinner.adapter = ArrayAdapter<String>(
            requireContext(),
            android.R.layout.simple_list_item_1,
            songManager.artists
        )
        artistSpinner.setSelection(spinnerPositionArtist)
        artistSpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                adapterView?.apply {
                    if (position != spinnerPositionArtist) {
                        spinnerPositionArtist = position
                        val selectedItem = adapterView.getItemAtPosition(position).toString()
                        songAdapter.submitList(songManager.sortedByArtist(selectedItem))
                        songAdapter.notifyDataSetChanged()
                    }
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                //Nothing
            }

        }
    }

    private fun setupRecyclerView() {
        songAdapter =
            SongListAdapter(
                SongDiffUtilItemCallback(),
                ItemClickListener<Song> { song: Song, position: Int ->

                    Intent(BROADCAST_ACTION_SELECT_SONG).apply {
                        putExtra(EXTRA_SONG_FROM_LIST, song)
                        addFlags(Intent.FLAG_RECEIVER_FOREGROUND)
                        requireContext().sendBroadcast(this)
                    }
                    SongManager.clear()
                })
        songsRecyclerView.adapter = songAdapter
    }

    override fun onLoaderReset(loader: Loader<SongManager>) {}
    override fun onCreateLoader(id: Int, args: Bundle?): Loader<SongManager> {
        return SongManagerLoader(this.requireContext(), this.requireActivity().contentResolver)
    }

    override fun onLoadFinished(loader: Loader<SongManager>, songManager: SongManager?) {
        songManager?.apply {
            mSongManager = this
            Timber.d("mSongManager.hashCode() = ${mSongManager.hashCode()}")
            if (mSongManager.isActive) {

                songAdapter.submitList(mSongManager.songs)
                setupSpinners(mSongManager)

            } else {
                Toast.makeText(requireContext(), "Something went wrong, please restart the application", Toast.LENGTH_SHORT).show()
            }
        }
    }

}

class SongManagerLoader(
    context: Context,
    private val contentResolver: ContentResolver
) :
    AsyncTaskLoader<SongManager>(context) {

    override fun onStartLoading() {
        super.onStartLoading()
        forceLoad() // without calling this loadInBackground will not execute
    }

    override fun loadInBackground(): SongManager? {
        return SongManager.getInstance(contentResolver)
    }
}
