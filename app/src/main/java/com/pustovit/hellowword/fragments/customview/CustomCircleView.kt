package com.pustovit.hellowword.fragments.customview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.util.AttributeSet
import android.view.View
import com.pustovit.hellowword.R
import kotlin.math.roundToInt

class CustomCircleView(context: Context, attributeSet: AttributeSet) : View(context, attributeSet) {

    //paint for drawing custom view
    private val mPaint: Paint = Paint()

    private val mCircleRadius: Float
    private var mBackgroundColor: Int

    init {

        val typedArray = context.theme.obtainStyledAttributes(
            attributeSet,
            R.styleable.CustomCircleView, 0, 0
        )

        try {
            // Если в атрибуте android:background не цвет или атрибут не объявлен,
            // то устанавливаем черный цвет.
            mBackgroundColor = try {
                val backgroundDrawable =
                    typedArray.getDrawable(R.styleable.CustomCircleView_android_background)
                if (backgroundDrawable != null) {
                    val colorDrawable: ColorDrawable = backgroundDrawable as ColorDrawable
                    colorDrawable.color
                } else {
                    Color.BLACK
                }
            } catch (ex: ClassCastException) {
                ex.printStackTrace()
                Color.BLACK
            }

            mCircleRadius = typedArray.getDimension(R.styleable.CustomCircleView_circleRadius, 100f)

        } finally {
            typedArray.recycle();
        }

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val measuredWidth: Int = adjustSize((2 * mCircleRadius).roundToInt(), widthMeasureSpec)
        val measuredHeight: Int = adjustSize((2 * mCircleRadius).roundToInt(), heightMeasureSpec)
        setMeasuredDimension(measuredWidth, measuredHeight)
    }

    private fun adjustSize(contentSize: Int, measureSpec: Int): Int {
        val mode = MeasureSpec.getMode(measureSpec)
        val specSize = MeasureSpec.getSize(measureSpec)
        return when (mode) {
            MeasureSpec.EXACTLY -> specSize

            MeasureSpec.AT_MOST -> {
                if (contentSize < specSize) {
                    contentSize
                } else {
                    specSize
                }
            }
            MeasureSpec.UNSPECIFIED -> contentSize

            else -> contentSize
        }

    }

    override fun onDraw(canvas: Canvas?) {
        // Делаем бэкграунд прозрачным
        // Из стандартного атрибута android:background  мы уже достали цвет круга в init блоке.
        // Меняем стандартное поведение атрибута - убираем его, чтобы он наш круг был
        // заметен на фоне view
        background = null
        // Гугл подсказывает ещё такой путь background.setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.SRC),
        // но обнуление прекрасно работает, оставляем его.

        //Получаем координаты центра
        val viewWidthHalf = this.measuredWidth / 2
        val viewHeightHalf = this.measuredHeight / 2

        mPaint.style = Paint.Style.FILL;
        mPaint.isAntiAlias = true
        mPaint.color = mBackgroundColor

        canvas?.drawCircle(
            viewWidthHalf.toFloat(),
            viewHeightHalf.toFloat(),
            mCircleRadius,
            mPaint
        )
    }

}