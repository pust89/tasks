package com.pustovit.hellowword.fragments.corecomponents.contentprovider

import android.content.ContentProvider
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.database.SQLException
import android.net.Uri
import androidx.sqlite.db.SupportSQLiteQuery
import androidx.sqlite.db.SupportSQLiteQueryBuilder
import com.pustovit.hellowword.fragments.corecomponents.asDatabaseSong
import com.pustovit.hellowword.fragments.corecomponents.contentprovider.SongsContract.Companion.CONTENT_TYPE
import com.pustovit.hellowword.fragments.corecomponents.contentprovider.database.AppSongsDatabase
import com.pustovit.hellowword.fragments.corecomponents.contentprovider.database.dao.DatabaseSongDao
import timber.log.Timber


class SongProvider : ContentProvider() {

    private val uriMatcher: UriMatcher = buildUriMatcher()
    private lateinit var databaseSongDao: DatabaseSongDao

    companion object Contracts {
        // // Uri
        // authority
        const val CONTENT_AUTHORITY: String = "com.pustovit.hellowword.provider"
        val CONTENT_AUTHORITY_URI: Uri = Uri.parse("content://$CONTENT_AUTHORITY")

        private const val SONGS = 100
        private const val SONGS_ID = 101

    }

    private fun buildUriMatcher(): UriMatcher {

        val matcher = UriMatcher(UriMatcher.NO_MATCH)

        // com.pustovit.hellowword.provider/songs
        matcher.addURI(CONTENT_AUTHORITY, SongsContract.TABLE_NAME, SONGS)

        // com.pustovit.hellowword.provider/songs/8
        matcher.addURI(CONTENT_AUTHORITY, SongsContract.TABLE_NAME + "/#", SONGS_ID);
        return matcher
    }

    override fun onCreate(): Boolean {
        Timber.d("onCreate: called")
        this.context?.apply {
            databaseSongDao = AppSongsDatabase.getInstance(this).databaseSongDao()
            return true
        }
        return false
    }

    override fun query(
        uri: Uri,
        projection: Array<out String>?,
        selection: String?,
        selectionArgs: Array<out String>?,
        sortOrder: String?
    ): Cursor? {
        val match: Int = uriMatcher.match(uri)

        var cursor: Cursor? = null
        when (match) {
            SONGS -> {

                val query: SupportSQLiteQuery =
                    SupportSQLiteQueryBuilder.builder(SongsContract.TABLE_NAME)
                        .columns(projection)
                        .selection(selection, selectionArgs)
                        .orderBy(sortOrder)
                        .create()

                cursor = databaseSongDao.query(query)
            }

            SONGS_ID -> {
                val songId = SongsContract.getSongIdFromUri(uri)

                val query: SupportSQLiteQuery =
                    SupportSQLiteQueryBuilder.builder(SongsContract.TABLE_NAME)
                        .selection(SongsContract.COLUMN_ID, arrayOf(songId))
                        .create()
                cursor = databaseSongDao.query(query)

            }

            else -> throw  IllegalArgumentException("Unknown URI: $uri")
        }
        cursor.setNotificationUri(context?.contentResolver, uri);

        return cursor;
    }

    override fun getType(uri: Uri): String? {
        val match: Int = uriMatcher.match(uri)
        Timber.d("getType: match = $match")

        return when (match) {
            SONGS -> CONTENT_TYPE

            SONGS_ID -> SongsContract.CONTENT_ITEM_TYPE;

            else -> throw IllegalArgumentException("Unknown Uri: $uri");
        }
    }


    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        val match: Int = uriMatcher.match(uri)
        var returnUri: Uri? = null
        var recordId: Long = 0

        when (match) {

            SONGS -> {
                values?.apply {
                    recordId = databaseSongDao.insertDatabaseSong(this.asDatabaseSong())
                }
                returnUri = if (recordId >= 0) {
                    SongsContract.buildSongUriWithId(recordId)
                } else {
                    throw SQLException("Failed to insert into $uri")
                }
            }

            else -> throw IllegalArgumentException("Unknown Uri:$uri");
        }

        context?.contentResolver?.notifyChange(uri, null);
        return returnUri
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {

        val match: Int = uriMatcher.match(uri)
        var count: Int = 0

        when (match) {
            SONGS -> {
                val query: SupportSQLiteQuery =
                    SupportSQLiteQueryBuilder.builder(SongsContract.TABLE_NAME)
                        .selection(selection, selectionArgs)
                        .create()

                count = databaseSongDao.delete(query)
            }

            SONGS_ID -> {
                val songId = SongsContract.getSongIdFromUri(uri)

                count = databaseSongDao.deleteById(songId)
            }

            else -> throw IllegalArgumentException("Unknown Uri:$uri");
        }
        return count
    }

    override fun update(
        uri: Uri,
        values: ContentValues?,
        selection: String?,
        selectionArgs: Array<out String>?
    ): Int {

        val match: Int = uriMatcher.match(uri)

        var count: Int = 0

        when (match) {

            SONGS -> {

                val query: SupportSQLiteQuery =
                    SupportSQLiteQueryBuilder.builder(SongsContract.TABLE_NAME)
                        .selection(selection, selectionArgs)
                        .create()

                count = databaseSongDao.update(query)
            }

            SONGS_ID -> {
                values?.apply {
                    count = databaseSongDao.updateDatabaseSongs(this.asDatabaseSong())
                }
            }

            else -> throw IllegalArgumentException("Unknown Uri:$uri");
        }
        return count
    }

}
