package com.pustovit.hellowword.fragments.corecomponents

import androidx.recyclerview.widget.DiffUtil
import com.pustovit.hellowword.fragments.corecomponents.Song

class SongDiffUtilItemCallback : DiffUtil.ItemCallback<Song>() {
    override fun areItemsTheSame(oldItem: Song, newItem: Song): Boolean {
        return oldItem._id == newItem._id
    }

    override fun areContentsTheSame(oldItem: Song, newItem: Song): Boolean {
        return oldItem._id == newItem._id
    }
}