package com.pustovit.hellowword.fragments.news.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.pustovit.hellowword.R
import com.pustovit.hellowword.fragments.news.domain.News
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_selected_news.*


class SelectedNewsFragment : Fragment() {


    private val news by lazy<News?> {
        arguments?.let {
            it.getParcelable(ARG_PARAM_NEWS)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_selected_news, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        displayNews()
    }

    private fun displayNews() {
        news?.let {
            selected_news_title.text = it.title
            selected_news_source.text = it.sourceName
            selected_news_description.text = it.description

            Glide.with(this)
                .load(news?.urlToImage)
                .error(R.drawable.red_circle)
                .into(ivGlide)

            Picasso.get()
                .load(news?.urlToImage)
                .error(R.drawable.red_circle)
                .into(ivPicasso);

        }
    }

    companion object {

        private const val ARG_PARAM_NEWS = "selectedNews"

        fun newInstance(selectedNews: News) =
            SelectedNewsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_PARAM_NEWS, selectedNews)
                }
            }
    }
}