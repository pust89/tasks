package com.pustovit.hellowword.fragments.news.domain

import androidx.lifecycle.LiveData

interface NewsRepository {

    fun getNews(query: String): LiveData<ResultOf<List<News>>>

    fun onCleared()
}