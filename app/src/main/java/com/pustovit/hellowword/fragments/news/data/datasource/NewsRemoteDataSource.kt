package com.pustovit.hellowword.fragments.news.data.datasource

import com.pustovit.hellowword.fragments.news.data.network.NewsResponsePage
import io.reactivex.Observable


interface NewsRemoteDataSource {

    fun getNewsByNetwork(query:String): Observable<NewsResponsePage>


}