package com.pustovit.hellowword.fragments.news.presentation.dialogs

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.pustovit.hellowword.R

class ErrorDialogFragment : DialogFragment() {

    private val message by lazy {
        arguments?.let {
            it.getString(ARG_PARAM_ERROR_MSG)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
                .setTitle(R.string.error_dialog_title)
                .setMessage(message)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    companion object {
        const val TAG = "ErrorDialogFragment"
        private const val ARG_PARAM_ERROR_MSG = "ARG_PARAM_ERROR_MSG"
        fun newInstance(message: String) = ErrorDialogFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_PARAM_ERROR_MSG, message)
            }
        }
    }
}