package com.pustovit.hellowword.fragments.corecomponents.contentprovider.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.pustovit.hellowword.fragments.corecomponents.Song
import com.pustovit.hellowword.fragments.corecomponents.contentprovider.SongsContract

@Entity(
    tableName = SongsContract.TABLE_NAME,
)
data class DatabaseSong(
    @ColumnInfo(name = "_id")
    @PrimaryKey(autoGenerate = true)
    var _id: Long = 0,
    @ColumnInfo(name = "resourceId")val resourceId: Int,
    @ColumnInfo(name = "title")val title: String,
    @ColumnInfo(name = "artist")val artist: String,
    @ColumnInfo(name = "genre")val genre: String
)
