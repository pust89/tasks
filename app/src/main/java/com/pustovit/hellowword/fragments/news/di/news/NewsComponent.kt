package com.pustovit.hellowword.fragments.news.di.news

import com.pustovit.hellowword.fragments.news.presentation.NewsFragment
import dagger.Subcomponent

@NewsScope
@Subcomponent(modules = [NewsModule::class])
interface NewsComponent {

    // Factory to create instances of NewsComponent
    @Subcomponent.Factory
    interface Factory {
        fun create(): NewsComponent
    }

    fun inject(newsFragment: NewsFragment)
}