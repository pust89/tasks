package com.pustovit.hellowword.fragments.recview

import com.pustovit.hellowword.R

data class Figure(val id: Int, val resourceId: Int, val description: String) {
}

fun generateFigures(): List<Figure> {
    return ArrayList<Figure>().apply {
        add(Figure(id = 1, R.drawable.red_circle, "Red circle"))
        add(Figure(id = 2, R.drawable.yellow_rectangle, "Yellow rectangle"))
        add(Figure(id = 3, R.drawable.green_triangle, "Green triangle"))
        add(Figure(id = 4, R.drawable.blue_square, "Blue square"))
        add(Figure(id = 5, R.drawable.green_circle, "Green circle"))
        add(Figure(id = 6, R.drawable.black_circle, "Black circle"))
        add(Figure(id = 7, R.drawable.black_rectangle, "Black rectangle"))
        add(Figure(id = 8, R.drawable.black_triangle, "Black triangle"))
        add(Figure(id = 9, R.drawable.black_square, "Black square"))
        add(Figure(id = 10, R.drawable.yellow_circle, "Yellow circle"))
    }
}