package com.pustovit.hellowword.fragments.news.data

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import com.pustovit.hellowword.BuildConfig
import com.pustovit.hellowword.fragments.news.data.datasource.NewsRemoteDataSource
import com.pustovit.hellowword.fragments.news.data.network.ApiResponse
import com.pustovit.hellowword.fragments.news.data.network.NewsApi
import com.pustovit.hellowword.fragments.news.data.network.NewsResponsePage
import io.reactivex.Observable
import java.text.SimpleDateFormat
import java.util.*

class NewsRemoteDataSourceImpl( private val newsApi: NewsApi) : NewsRemoteDataSource {

    private val apiKey = BuildConfig.NEWS_API_KEY

    @SuppressLint("SimpleDateFormat")
    private val dateFormat = SimpleDateFormat("yyyy-MM-dd")
    private val currentDateStr: String = dateFormat.format(Date())


    override fun getNewsByNetwork(
        query: String
    ): Observable<NewsResponsePage> {
        return newsApi.getNewsObservable(query, currentDateStr, currentDateStr, apiKey)

    }
}