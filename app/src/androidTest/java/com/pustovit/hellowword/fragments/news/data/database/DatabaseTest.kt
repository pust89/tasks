package com.pustovit.hellowword.fragments.news.data.database

import android.content.Context
import android.os.Handler
import android.os.Looper
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.util.concurrent.Executors


@RunWith(AndroidJUnit4::class)
class DatabaseTest {

    private lateinit var databaseNewsDao: DatabaseNewsDao
    private lateinit var newsDatabase: AppNewsDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()

        newsDatabase = Room.inMemoryDatabaseBuilder(
            context, AppNewsDatabase::class.java
        ).build()
        databaseNewsDao = newsDatabase.databaseNewsDao()
    }

    @Test
    @Throws(Exception::class)
    fun insertNewsAndReadInList() {
        databaseNewsDao.insertNews(testNews_1)
        val newsFromDatabase = databaseNewsDao.getNewsByQuery(QUERY)
        assertThat(newsFromDatabase, equalTo(testNews_1))
    }

    @Test
    @Throws(Exception::class)
    fun insertNewsAndReadDistinctQueriesInList() {

        Handler(Looper.getMainLooper()).post {
            Executors.newSingleThreadExecutor().execute {
                databaseNewsDao.insertNews(testNews_2)
            }
            databaseNewsDao.getCachedQuery().observeForever {
                assertThat(it, equalTo(testQueries))
            }
        }
    }

    @Test
    @Throws(Exception::class)
    fun insertNewsAndClearTable() {
        databaseNewsDao.insertNews(testNews_1)
        databaseNewsDao.clearDatabase()

        val newsFromDatabase = databaseNewsDao.getNewsByQuery(QUERY)
        assertThat(newsFromDatabase, equalTo(emptyList()))
    }


    @After
    @Throws(IOException::class)
    fun closeDb() {
        newsDatabase.close()
    }


    companion object {

        private val testQueries: List<String> =
            listOf("software", "cars", "football")

        private const val QUERY = "software"

        private val testNews_1 = listOf<DatabaseNews>(
            DatabaseNews(
                1, QUERY, "author1", "content1", "description1",
                "2020-10-26", "sourceName1", "title1", "url1",
                "urlToImage1"
            ),
            DatabaseNews(
                2, QUERY, "author2", "content2", "description2",
                "2020-10-26", "sourceName2", "title2", "url2",
                "urlToImage2"
            ),
            DatabaseNews(
                3, QUERY, "author3", "content3", "description3",
                "2020-10-26", "sourceName3", "title3", "url3",
                "urlToImage3"
            )
        )


        private val testNews_2 = listOf<DatabaseNews>(
            DatabaseNews(
                1, "software", "author1", "content1", "description1",
                "publishedAt1", "sourceName1", "title1", "url1",
                "urlToImage1"
            ),
            DatabaseNews(
                2, "cars", "author2", "content2", "description2",
                "publishedAt2", "sourceName2", "title2", "url2",
                "urlToImage2"
            ),
            DatabaseNews(
                3, "football", "author3", "content3", "description3",
                "publishedAt3", "sourceName3", "title3", "url3",
                "urlToImage3"
            )
        )

    }

}