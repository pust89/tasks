package com.pustovit.hellowword.fragments.corecomponents.contentprovider.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.pustovit.hellowword.R
import com.pustovit.hellowword.fragments.corecomponents.contentprovider.SongsContract
import com.pustovit.hellowword.fragments.corecomponents.contentprovider.database.dao.DatabaseSongDao
import com.pustovit.hellowword.fragments.corecomponents.contentprovider.database.entity.DatabaseSong
import java.util.concurrent.Executors


@Database(
    entities = [DatabaseSong::class],
    version = 1,
    exportSchema = false
)
abstract class AppSongsDatabase() : RoomDatabase() {

    abstract fun databaseSongDao(): DatabaseSongDao

    companion object {
        private const val DB_NAME = "songs_database"

        @Volatile
        private var INSTANCE: AppSongsDatabase? = null

        fun getInstance(context: Context): AppSongsDatabase {
            synchronized(AppSongsDatabase::class.java) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppSongsDatabase::class.java,
                        DB_NAME
                    )
                        .fallbackToDestructiveMigration()
                        .addCallback(MyRoomCallback(context))
                        .build()

                    INSTANCE = instance
                }
                return instance
            }
        }

    }


}

class MyRoomCallback(context: Context) : RoomDatabase.Callback() {

    private val songs: Array<ContentValues>

    init {
        songs = feelDatabase(context)
    }

    override fun onCreate(db: SupportSQLiteDatabase) {
        super.onCreate(db)

        Executors.newSingleThreadExecutor().execute {
            for (i in songs.indices) {
                db.insert(SongsContract.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, songs[i])
            }
        }
    }

    private fun feelDatabase(context: Context): Array<ContentValues> {

        val resIdArr = context.resources.getIntArray(R.array.array_song_res_id)
        val artistArr = context.resources.getStringArray(R.array.array_song_artists)
        val titleArr = context.resources.getStringArray(R.array.array_song_titles)
        val genreArr = context.resources.getStringArray(R.array.array_song_genres)

        return Array(14) {
            return@Array ContentValues().apply {
                put(SongsContract.COLUMN_RESOURCE_ID, resIdArr[it])
                put(SongsContract.COLUMN_RESOURCE_ID, resIdArr[it])
                put(SongsContract.COLUMN_ARTIST, artistArr[it])
                put(SongsContract.COLUMN_TITLE, titleArr[it])
                put(SongsContract.COLUMN_GENRE, genreArr[it])
            }
        }
    }
}
