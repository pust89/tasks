package com.pustovit.hellowword.fragments.news.di.core.module

import com.pustovit.hellowword.fragments.news.data.NewsRemoteDataSourceImpl
import com.pustovit.hellowword.fragments.news.data.datasource.NewsRemoteDataSource
import com.pustovit.hellowword.fragments.news.data.network.NewsApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RemoteDataModule {

    @Singleton
    @Provides
    fun provideNewsRemoteDataSource(newsApi: NewsApi): NewsRemoteDataSource {
        return NewsRemoteDataSourceImpl(newsApi)
    }
}