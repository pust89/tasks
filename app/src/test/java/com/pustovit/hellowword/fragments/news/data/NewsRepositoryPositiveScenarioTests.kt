package com.pustovit.hellowword.fragments.news.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.timeout
import com.nhaarman.mockitokotlin2.verify
import com.pustovit.hellowword.fragments.news.data.database.DatabaseNews
import com.pustovit.hellowword.fragments.news.data.datasource.NewsLocalDataSource
import com.pustovit.hellowword.fragments.news.data.datasource.NewsRemoteDataSource
import com.pustovit.hellowword.fragments.news.data.network.Article
import com.pustovit.hellowword.fragments.news.data.network.NewsResponsePage
import com.pustovit.hellowword.fragments.news.data.network.Source
import com.pustovit.hellowword.fragments.news.data.pref.NewsPreferences
import com.pustovit.hellowword.fragments.news.domain.News
import com.pustovit.hellowword.fragments.news.domain.NewsRepository
import com.pustovit.hellowword.fragments.news.domain.ResultOf
import io.reactivex.Observable
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import java.util.*


@RunWith(MockitoJUnitRunner::class)
class NewsRepositoryPositiveScenarioTests {


    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var localDataSource: NewsLocalDataSource

    @Mock
    private lateinit var remoteDataSource: NewsRemoteDataSource

    @Mock
    private lateinit var newsPreferences: NewsPreferences

    @Mock
    private lateinit var newsObserver: Observer<ResultOf<List<News>>>

    private var newsRepository: NewsRepository? = null


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @After
    fun tearDown() {
        newsRepository?.onCleared()
        newsRepository = null
    }

    /**
     * Тест кейс:
     * 1) Запрашиваем новости по запросу QUERY.
     * 2) В методе getNews определяется, что данные устарели (прошло больше 1 мин. с предыдущего запроса).
     * 3) Кеш очищается.(Удаляются все данные из бд)
     * 4) Происходит запрос к api, который возращает NewsResponsePage с 4 новостями.
     *    - внутри репозитория происходит фильтрация полученных новостей по длине тайтла
     *    - к тайтлу добавляется фраза "Filtered. "
     * 5) Убеждаемся, что произошел вызово метода по очистки кеша ( localDataSource.clearCache() ).
     * 6) Сравниваем, что сохраненный в кеш список новостей (List<DatabaseNews>) соответствует ожидаемому.
     * 7) Сравниваем, что тестовый Observer получил ожидаемы  результат (ResultOf.Success(List<News>)).
     */
    @Test
    fun refreshNewsAndCacheTest() {

        /*Answer from api*/
        val newsResponsePage = NewsResponsePage(testArticles)

        /* Подменяем ответ от remoteDataSource.getNewsByNetwork(QUERY)*/
        Mockito.`when`(remoteDataSource.getNewsByNetwork(QUERY))
                .thenReturn(Observable.just(newsResponsePage))

        /* Со времени предыдущего запрос прошло больше 1 мин.*/
        Mockito.`when`(newsPreferences.getLastQueryTime()).thenReturn(Date().time - 70000)

        /* Возвращаем список новостных тем, которые содержаться в кеше */
        Mockito.`when`(localDataSource.getCachedQueries())
                .thenReturn(MutableLiveData<List<String>>(cachedQueries))

        newsRepository =
                NewsRepositoryImpl(remoteDataSource, localDataSource, newsPreferences)

        /* Выполняем запрос и подписывам LiveData на тестовый Observer*/
        newsRepository!!.getNews(QUERY).observeForever(newsObserver)

        verify(localDataSource, timeout(1000)).clearCache()
        verify(remoteDataSource, timeout(1000)).getNewsByNetwork(QUERY)
        verify(localDataSource, timeout(1000)).saveNews(expectedNewsToBeCached)
        verify(newsObserver, timeout(1000)).onChanged(expectedSuccessfulResult)
    }

    /**
     * Тест кейс:
     * 1) Запрашиваем новости по запросу QUERY.
     * 2) В методе getNews определяется, что со времени предыдущего запрос прошло меньше 1 минуты,
     *    но запрос QUERY не содержиться в списке кэшированных запросов.
     * 3) Происходит запрос к api, который возращает NewsResponsePage с 4 новостями.
     *    - внутри репозитория происходит фильтрация полученных новостей по длине тайтла
     *    - к тайтлу добавляется фраза "Filtered. "
     * 4) Сравниваем, что сохраненный в кеш список новостей (List<DatabaseNews>) соответствует ожидаемому.
     * 5) Сравниваем, что тестовый Observer получил ожидаемы  результат (ResultOf.Success(List<News>)).
     */
    @Test
    fun getNewsFromNetworkAndCacheThemTest() {

        /*Answer from api*/
        val newsResponsePage = NewsResponsePage(testArticles)

        /* Подменяем ответ от remoteDataSource.getNewsByNetwork(QUERY) */
        Mockito.`when`(remoteDataSource.getNewsByNetwork(QUERY))
                .thenReturn(Observable.just(newsResponsePage))

        /* Со времени предыдущего запрос прошло меньше 1 мин.*/
        Mockito.`when`(newsPreferences.getLastQueryTime()).thenReturn(Date().time)

        /* Возвращаем пустой список новостных тем, которые содержаться в кеше. */
        Mockito.`when`(localDataSource.getCachedQueries())
                .thenReturn(MutableLiveData<List<String>>(emptyList()))

        newsRepository =
                NewsRepositoryImpl(remoteDataSource, localDataSource, newsPreferences)

        /* Выполняем запрос и подписывам LiveData на тестовый Observer */
        newsRepository!!.getNews(QUERY).observeForever(newsObserver)

        verify(remoteDataSource, timeout(1000)).getNewsByNetwork(QUERY)
        verify(localDataSource, timeout(1000)).saveNews(expectedNewsToBeCached)
        verify(newsObserver, timeout(1000)).onChanged(expectedSuccessfulResult)
    }

    /**
     * Тест кейс:
     * 1) Запрашиваем новости по запросу QUERY.
     * 2) В методе getNews определяется, что запрос кэшированн и данные не устарели.
     * 3) Происходит запрос к localDataSource, который возращает List<DatabaseNews> с 2 новостями.
     * 4) Убеждаемся, что произошел вызов метода getNews у объекта localDataSource.
     * 5) Сравниваем, что тестовый Observer получил ожидаемы  результат (ResultOf.Success(List<News>)).
     */
    @Test
    fun getNewsFromCacheTest() {

        /* Со времени предыдущего запрос прошло меньше 1 мин.*/
        Mockito.`when`(newsPreferences.getLastQueryTime()).thenReturn(Date().time)

        /* Возвращаем список новостных тем, которые содержаться в кеше.
        *  Запрос  QUERY содержиться в нём. */
        Mockito.`when`(localDataSource.getCachedQueries())
                .thenReturn(MutableLiveData<List<String>>(cachedQueries))

        /* Подменяем ответ от localDataSource.getNews(QUERY). */
        Mockito.`when`(localDataSource.getNews(QUERY))
                .thenReturn(newsFromCache)

        newsRepository =
                NewsRepositoryImpl(remoteDataSource, localDataSource, newsPreferences)

        /* Выполняем запрос и подписывам LiveData на тестовый Observer. */
        newsRepository!!.getNews(QUERY).observeForever(newsObserver)

        verify(localDataSource, timeout(1000)).getNews(QUERY)
        verify(newsObserver, timeout(1000)).onChanged(expectedSuccessfulResult)
    }

    companion object {
        private val cachedQueries: List<String> =
                listOf("software", "cars", "android", "football", "covid")
        private const val QUERY = "software"

        val testArticles = listOf<Article>(
                Article(
                        "author1", "content1", "description1",
                        "2020-10-26", Source("sourceName1"), "title1111111111111111", "url1",
                        "urlToImage1"
                ),
                Article(
                        "author2", "content2", "description2",
                        "2020-10-26", Source("sourceName2"), "title2", "url2",
                        "urlToImage2"
                ),
                Article(
                        "author3", "content3", "description3",
                        "2020-10-26", Source("sourceName3"), "title3333333333333333", "url3",
                        "urlToImage3"
                ),
                Article(
                        "author4", "content4", "description4",
                        "2020-10-26", Source("sourceName4"), "title4", "url4",
                        "urlToImage4"
                )
        )


        val expectedNewsToBeCached = listOf<DatabaseNews>(
                DatabaseNews(
                        0, QUERY, "author1", "content1", "description1",
                        "2020-10-26", "sourceName1", "Filtered. title1111111111111111", "url1",
                        "urlToImage1"
                ),
                DatabaseNews(
                        0, QUERY, "author3", "content3", "description3",
                        "2020-10-26", "sourceName3", "Filtered. title3333333333333333", "url3",
                        "urlToImage3"
                )
        )

        val expectedSuccessfulResult = ResultOf.Success(
                listOf<News>(
                        News(
                                "author1", "content1", "description1",
                                "2020-10-26", "sourceName1", "Filtered. title1111111111111111", "url1",
                                "urlToImage1"
                        ),
                        News(
                                "author3", "content3", "description3",
                                "2020-10-26", "sourceName3", "Filtered. title3333333333333333", "url3",
                                "urlToImage3"
                        )
                )
        )

        val newsFromCache = listOf<DatabaseNews>(
                DatabaseNews(
                        1, QUERY, "author1", "content1", "description1",
                        "2020-10-26", "sourceName1", "Filtered. title1111111111111111", "url1",
                        "urlToImage1"
                ),
                DatabaseNews(
                        2, QUERY, "author3", "content3", "description3",
                        "2020-10-26", "sourceName3", "Filtered. title3333333333333333", "url3",
                        "urlToImage3"
                )
        )
    }
}