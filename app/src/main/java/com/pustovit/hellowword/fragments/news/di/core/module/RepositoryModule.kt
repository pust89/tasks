package com.pustovit.hellowword.fragments.news.di.core.module

import com.pustovit.hellowword.fragments.news.data.NewsRepositoryImpl
import com.pustovit.hellowword.fragments.news.data.datasource.NewsLocalDataSource
import com.pustovit.hellowword.fragments.news.data.datasource.NewsRemoteDataSource
import com.pustovit.hellowword.fragments.news.data.pref.NewsPreferences
import com.pustovit.hellowword.fragments.news.domain.NewsRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun provideNewsRepository(
        remoteDataSource: NewsRemoteDataSource,
        localDataSource: NewsLocalDataSource, preferences: NewsPreferences
    ): NewsRepository {
        return NewsRepositoryImpl(remoteDataSource, localDataSource, preferences)
    }

}