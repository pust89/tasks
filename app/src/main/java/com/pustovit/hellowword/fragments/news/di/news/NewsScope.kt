package com.pustovit.hellowword.fragments.news.di.news

import javax.inject.Scope

@Scope
@MustBeDocumented
@Retention(value = AnnotationRetention.RUNTIME)
annotation class NewsScope