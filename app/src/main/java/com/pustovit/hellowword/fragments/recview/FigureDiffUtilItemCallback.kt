package com.pustovit.hellowword.fragments.recview

import androidx.recyclerview.widget.DiffUtil

class  FigureDiffUtilItemCallback : DiffUtil.ItemCallback<Figure>() {
    override fun areItemsTheSame(oldItem: Figure, newItem: Figure): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Figure, newItem: Figure): Boolean {
        return oldItem.id == newItem.id
    }
}