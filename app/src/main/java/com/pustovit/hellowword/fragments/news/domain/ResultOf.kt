package com.pustovit.hellowword.fragments.news.domain

import com.pustovit.hellowword.fragments.news.data.network.ApiResponse

sealed class ResultOf<out T> {

    data class Success<out R>(val value: R): ResultOf<R>()

    data class Failure(
        val throwable: Throwable?
    ): ResultOf<Nothing>()

    object Empty : ResultOf<Nothing>()
}