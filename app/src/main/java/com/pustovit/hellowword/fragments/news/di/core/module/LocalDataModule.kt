package com.pustovit.hellowword.fragments.news.di.core.module

import com.pustovit.hellowword.fragments.news.data.NewsLocalDataSourceImpl
import com.pustovit.hellowword.fragments.news.data.database.DatabaseNewsDao
import com.pustovit.hellowword.fragments.news.data.datasource.NewsLocalDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LocalDataModule {

    @Singleton
    @Provides
    fun provideNewsLocalDataSource(databaseNewsDao: DatabaseNewsDao): NewsLocalDataSource {
        return NewsLocalDataSourceImpl(databaseNewsDao)
    }
}