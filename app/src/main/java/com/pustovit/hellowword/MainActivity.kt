package com.pustovit.hellowword


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import com.pustovit.hellowword.navigation.MyNavigator
import com.pustovit.hellowword.navigation.NavigatorHolderProvider
import com.pustovit.hellowword.navigation.RouterProvider
import com.pustovit.hellowword.navigation.Screens
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator


class MainActivity : AppCompatActivity() {

    private val navigator: SupportAppNavigator = MyNavigator(this,R.id.main_activity_container)

    private val navigatorHolder: NavigatorHolder by lazy {
        (application as NavigatorHolderProvider).getNavigatorHolder()
    }

    val router: Router by lazy {
        (application as RouterProvider).getRouter()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        setApplicationNavigation(savedInstanceState)

        redirectionToFragments(intent)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        redirectionToFragments(intent)
    }

    private fun setApplicationNavigation(savedInstanceState: Bundle?) {
        // Set home fragment
        if (savedInstanceState == null) {
            router.newRootScreen(Screens.HomeFragmentScreen())
        }

        val actionBarDrawerToggle: ActionBarDrawerToggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()

        navigationView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_hello -> router.navigateTo(Screens.HelloFragmentScreen())
                R.id.nav_edittext -> router.navigateTo(Screens.EditTextFragmentScreen())
                R.id.nav_recycler -> router.navigateTo(Screens.RecyclerFragmentScreen())
                R.id.nav_orient_tablet -> router.navigateTo(Screens.OrientationTabletFragmentScreen())
                R.id.nav_core -> router.navigateTo(Screens.CoreFragmentScreen())
                R.id.nav_retrofit -> router.navigateTo(Screens.NewsFragmentScreen())
                R.id.nav_custom_view -> router.navigateTo(Screens.CustomViewFragmentScreen())
                R.id.nav_google_maps -> router.navigateTo(Screens.MapsFragmentScreen())

                else -> return@setNavigationItemSelectedListener false
            }
            drawerLayout.closeDrawer(Gravity.LEFT)
            return@setNavigationItemSelectedListener true
        }
    }


    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }


    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    companion object {
        fun getIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }

        const val ACTION_SHOW_CORE_FRAGMENT = "com.pustovit.hellowword.SHOW_CORE_FRAGMENT"
    }

    private fun redirectionToFragments(intent: Intent?) {
        intent?.let {
            when (it.action) {
                ACTION_SHOW_CORE_FRAGMENT -> router.navigateTo(Screens.CoreFragmentScreen())
                else -> return
            }
        }
    }
}
