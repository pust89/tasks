package com.pustovit.hellowword.fragments.customview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.pustovit.hellowword.R
import com.pustovit.hellowword.common.BaseFragment


class CustomViewFragment : BaseFragment() {

    override fun getTitleResId(): Int {
        return R.string.title_custom_view_fragment
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_custom_view, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //

    }

    companion object {
        fun newInstance() = CustomViewFragment()
    }
}