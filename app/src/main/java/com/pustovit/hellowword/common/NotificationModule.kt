package com.pustovit.hellowword.common

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.pustovit.hellowword.MainActivity
import com.pustovit.hellowword.R
import com.pustovit.hellowword.fragments.corecomponents.Song


object NotificationModule {

    const val NOTIFICATION_ID_FOR_PLAYER_SERVICE: Int = 6666
    private const val NOTIFICATION_CHANNEL_ID = "com.pustovit.andersen.channel"

    fun postNotificationSong(context: Context, song: Song) {
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
        notificationManager?.let {
            it.notify(NOTIFICATION_ID_FOR_PLAYER_SERVICE, getNotification(context, song).apply {
            })
        }
    }

    fun getBaseNotificationForPlayerService(context: Context): Notification {
       return getNotification(context, null)
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(context: Context) {
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channel = NotificationChannel(
            NOTIFICATION_CHANNEL_ID, "My channel",
            NotificationManager.IMPORTANCE_LOW
        )
        channel.setDescription("My channel description");
        channel.setLightColor(Color.RED);
        notificationManager.createNotificationChannel(channel);
    }

    private fun getNotification(context: Context, song: Song?): Notification {

        val title: String = song?.title ?: ""
        val text: String = song?.artist ?: ""

        val intent = Intent(context, MainActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            this.action = MainActivity.ACTION_SHOW_CORE_FRAGMENT
        }

        val pendingIntent = PendingIntent.getActivity(
            context,
            0,
            intent,
            PendingIntent.FLAG_CANCEL_CURRENT
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(context)
            val builder: NotificationCompat.Builder = NotificationCompat.Builder(
                context, NOTIFICATION_CHANNEL_ID
            ).setSmallIcon(R.drawable.green_circle)
                .setContentTitle(title)
                .setContentText(text)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setOngoing(true)
            return builder.build()

        } else {
            val builder: NotificationCompat.Builder =
                NotificationCompat.Builder(context).setSmallIcon(R.drawable.green_circle)
                    .setContentTitle(title)
                    .setContentText(text)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationManagerCompat.IMPORTANCE_LOW)
                    .setOngoing(true)
            return builder.build()

        }
    }
}