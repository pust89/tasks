package com.pustovit.hellowword.fragments.news.presentation.adapter

import androidx.recyclerview.widget.DiffUtil
import com.pustovit.hellowword.fragments.news.domain.News

class NewsDiffUtilItemCallback : DiffUtil.ItemCallback<News>() {
    override fun areItemsTheSame(oldItem: News, newItem: News): Boolean {
        return oldItem.equals(newItem)
    }

    override fun areContentsTheSame(oldItem: News, newItem: News): Boolean {
        return oldItem.equals(newItem)

    }
}