package com.pustovit.hellowword.fragments.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pustovit.hellowword.R
import com.pustovit.hellowword.common.BaseFragment

class HomeFragment : BaseFragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun getTitleResId(): Int {
        return R.string.app_name
    }


    companion object {
        fun newInstance() = HomeFragment()
    }
}