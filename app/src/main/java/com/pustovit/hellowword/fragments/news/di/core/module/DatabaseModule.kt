package com.pustovit.hellowword.fragments.news.di.core.module

import android.content.Context
import com.pustovit.hellowword.fragments.news.data.database.AppNewsDatabase
import com.pustovit.hellowword.fragments.news.data.database.DatabaseNewsDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabaseNewsDao(appNewsDatabase: AppNewsDatabase): DatabaseNewsDao {
        return appNewsDatabase.databaseNewsDao()
    }

    @Singleton
    @Provides
    fun provideAppNewsDatabase(context: Context): AppNewsDatabase {
        return AppNewsDatabase.getInstance(context)
    }
}