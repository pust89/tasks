package com.pustovit.hellowword.fragments.news.data

import androidx.lifecycle.LiveData
import com.pustovit.hellowword.fragments.news.data.database.DatabaseNews
import com.pustovit.hellowword.fragments.news.data.database.DatabaseNewsDao
import com.pustovit.hellowword.fragments.news.data.datasource.NewsLocalDataSource

class NewsLocalDataSourceImpl(private val databaseNewsDao: DatabaseNewsDao) : NewsLocalDataSource {

    override fun saveNews(databaseNews: List<DatabaseNews>) {
        databaseNewsDao.insertNews(databaseNews)
    }

    override fun getNews(query: String): List<DatabaseNews> {
        return databaseNewsDao.getNewsByQuery(query)
    }

    override fun getCachedQueries(): LiveData<List<String>> {
        return databaseNewsDao.getCachedQuery()
    }

    override fun clearCache() {
        databaseNewsDao.clearDatabase()
    }
}