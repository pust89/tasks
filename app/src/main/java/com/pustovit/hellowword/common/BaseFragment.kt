package com.pustovit.hellowword.common

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.pustovit.hellowword.MainActivity


abstract class BaseFragment() : Fragment() {

    val preferences: PreferencesModule by lazy {
        PreferencesModule
    }

    val router by lazy {
        (requireActivity() as MainActivity).router
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.title = getString(getTitleResId())
    }

    abstract fun getTitleResId(): Int
}
