package com.pustovit.hellowword.fragments.news.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.pustovit.hellowword.fragments.news.domain.News
import com.pustovit.hellowword.fragments.news.domain.NewsRepository
import com.pustovit.hellowword.fragments.news.domain.ResultOf
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.inOrder
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class NewsFragmentViewModelTest {

    companion object {
        private val testTopics: List<String> =
            listOf("software", "cars", "android", "football", "covid")

        private val testNews = listOf<News>(
            News(
                "author1", "content1", "description1",
                "2020-10-26", "sourceName1", "title1", "url1",
                "urlToImage1"
            ),
            News(
                "author2", "content2", "description2",
                "2020-10-26", "sourceName2", "title2", "url2",
                "urlToImage2"
            ),
            News(
                "author3", "content3", "description3",
                "2020-10-26", "sourceName3", "title3", "url3",
                "urlToImage3"
            )
        )

        private const val FAILURE_THROWABLE_MSG = "FakeThrowableMsg"

        private const val SELECTED_QUERY = "football"
        private const val SELECTED_POSITION = 3

    }

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var newsViewModel: NewsFragmentViewModel

    @Mock
    private lateinit var newsRepository: NewsRepository

    @Mock
    private lateinit var newsObserver: Observer<List<News>>




    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        newsViewModel = NewsFragmentViewModel(newsRepository)
        newsViewModel.newsLiveData.observeForever(newsObserver)


        Mockito.`when`(newsRepository.getNews(any())).then {
            MutableLiveData<ResultOf<List<News>>>().apply {
                postValue(ResultOf.Success(testNews))
            }
        }
    }


    @Test
    fun topicsListTest() {
        assertEquals(testTopics, newsViewModel.topics)
    }

    @Test
    fun receivedNewsSuccess() {
        newsViewModel.onSelectTopic(SELECTED_POSITION)
        verify(newsObserver).onChanged(testNews)
    }

    @Test
    fun receivedNewsFailure() {
        Mockito.`when`(newsRepository.getNews(any())).then {
            MutableLiveData<ResultOf<List<News>>>().apply {
                postValue(ResultOf.Failure(Throwable(FAILURE_THROWABLE_MSG)))
            }
        }

        val errorDialogLiveDataObserver: Observer<String?> = mock()
        newsViewModel.errorDialogLiveData.observeForever(errorDialogLiveDataObserver)

        newsViewModel.onSelectTopic(SELECTED_POSITION)
        verify(errorDialogLiveDataObserver).onChanged(FAILURE_THROWABLE_MSG)
    }

    @Test
    fun onSelectTopicTest() {
        val positionObserver: Observer<Int> = mock()
        newsViewModel.selectedTopicPositionLiveData.observeForever(positionObserver)

        val titleObserver: Observer<String> = mock()
        newsViewModel.titleLiveData.observeForever(titleObserver)

        newsViewModel.onSelectTopic(SELECTED_POSITION)
        verify(positionObserver).onChanged(SELECTED_POSITION)
        verify(titleObserver).onChanged(SELECTED_QUERY)
    }


    @Test
    fun positiveScenarioWhenLoadNewDataTest() {
        val positionObserver: Observer<Int> = mock()
        newsViewModel.selectedTopicPositionLiveData.observeForever(positionObserver)

        val titleObserver: Observer<String> = mock()
        newsViewModel.titleLiveData.observeForever(titleObserver)

        val downloadingObserver: Observer<Boolean> = mock()
        newsViewModel.downloadingDialogLiveData.observeForever(downloadingObserver)

        newsViewModel.onSelectTopic(SELECTED_POSITION)
        verify(positionObserver).onChanged(SELECTED_POSITION)
        verify(titleObserver).onChanged(SELECTED_QUERY)

        val inOrder = inOrder(downloadingObserver)
        inOrder.verify(downloadingObserver).onChanged(true)
        inOrder.verify(downloadingObserver).onChanged(false)

        verify(newsObserver).onChanged(testNews)
    }

    @Test
    fun negativeScenarioWhenLoadNewDataTest() {

        Mockito.`when`(newsRepository.getNews(any())).then {
            MutableLiveData<ResultOf<List<News>>>().apply {
                postValue(ResultOf.Failure(Throwable(FAILURE_THROWABLE_MSG)))
            }
        }

        val positionObserver: Observer<Int> = mock()
        newsViewModel.selectedTopicPositionLiveData.observeForever(positionObserver)

        val titleObserver: Observer<String> = mock()
        newsViewModel.titleLiveData.observeForever(titleObserver)

        val downloadingObserver: Observer<Boolean> = mock()
        newsViewModel.downloadingDialogLiveData.observeForever(downloadingObserver)

        val errorDialogLiveDataObserver: Observer<String?> = mock()
        newsViewModel.errorDialogLiveData.observeForever(errorDialogLiveDataObserver)

        newsViewModel.onSelectTopic(SELECTED_POSITION)
        verify(positionObserver).onChanged(SELECTED_POSITION)
        verify(titleObserver).onChanged(SELECTED_QUERY)

        val inOrder = inOrder(downloadingObserver)
        inOrder.verify(downloadingObserver).onChanged(true)
        inOrder.verify(downloadingObserver).onChanged(false)

        verify(errorDialogLiveDataObserver).onChanged(FAILURE_THROWABLE_MSG)
    }
}




