package com.pustovit.hellowword.common

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.pustovit.hellowword.App
import com.pustovit.hellowword.fragments.corecomponents.Song


object PreferencesModule {

    private const val SP_SONG = "spSong"

    fun saveSongToSharedPreferences(context: Context, song: Song) {
        context.getSharedPreferences(App.PREFERENCE_KEY, AppCompatActivity.MODE_PRIVATE)
            .edit()
            .putString(SP_SONG, Gson().toJson(song))
            .apply()

    }

    fun getSongFromSharedPreferences(context: Context): Song? {
        val sPref = context.getSharedPreferences(App.PREFERENCE_KEY, AppCompatActivity.MODE_PRIVATE)
        val songGsonString = sPref.getString(SP_SONG, null)
        return if (songGsonString != null) {
            Gson().fromJson(songGsonString, Song::class.java)
        } else {
            null
        }
    }

}