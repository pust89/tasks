package com.pustovit.hellowword.fragments.news.data.pref

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class NewsPreferencesImpl(context: Context):NewsPreferences {
    private val sharedPreferences:SharedPreferences
    init {
        sharedPreferences = context.getSharedPreferences(NEWS_PREF_KEY, AppCompatActivity.MODE_PRIVATE)
    }

   override fun saveLastQueryTime(time:Long){
        sharedPreferences.edit().putLong(QUERY_TIME,time).apply()
    }

    override fun getLastQueryTime():Long{
      return  sharedPreferences.getLong(QUERY_TIME,Date().time)
    }

    companion object{
        private const val NEWS_PREF_KEY = "newsPrefKey"
        private const val QUERY_TIME = "queryTime"
    }
}