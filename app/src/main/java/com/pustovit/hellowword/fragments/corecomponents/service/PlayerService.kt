package com.pustovit.hellowword.fragments.corecomponents.service


import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.ServiceInfo
import android.os.Build
import android.os.IBinder
import com.pustovit.hellowword.common.NotificationModule
import com.pustovit.hellowword.common.NotificationModule.NOTIFICATION_ID_FOR_PLAYER_SERVICE
import com.pustovit.hellowword.fragments.corecomponents.CoreFragment
import com.pustovit.hellowword.fragments.corecomponents.Song
import timber.log.Timber


class PlayerService : Service() {

    private val myMediaPlayer: MyMediaPlayer = MyMediaPlayer()

    private val notificationModule = NotificationModule

    override fun onCreate() {
        super.onCreate()
        setupService()
        Timber.d("onCreate: called")
        isActive = true
    }

    private fun setupService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            startForeground(
                NOTIFICATION_ID_FOR_PLAYER_SERVICE,
                notificationModule.getBaseNotificationForPlayerService(this),
                ServiceInfo.FOREGROUND_SERVICE_TYPE_MEDIA_PLAYBACK
            )
        } else {
            startForeground(
                NOTIFICATION_ID_FOR_PLAYER_SERVICE,
                notificationModule.getBaseNotificationForPlayerService(this)
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("onDestroy: called")
        isActive = false
        myMediaPlayer.clear()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Timber.d("onStartCommand: intent?.action = ${intent?.action}")

        intent?.apply {
            when (action) {

                ACTION_PLAY -> {
                    val songFromIntent = intent.getParcelableExtra<Song>(EXTRA_SONG)
                    songFromIntent?.apply {
                        myMediaPlayer.play(this@PlayerService, songFromIntent)
                        sendBroadcast(Intent(CoreFragment.BROADCAST_ACTION).apply {
                            putExtra(EXTRA_SONG, songFromIntent)
                        })
                    }
                }

                ACTION_PAUSE -> {
                    myMediaPlayer.pause()
                    sendBroadcast(Intent(CoreFragment.BROADCAST_ACTION).apply {
                        putExtra(EXTRA_SONG, myMediaPlayer.currentSong)
                    })
                }

                ACTION_STOP -> {
                    myMediaPlayer.stop()
                    sendBroadcast(Intent(CoreFragment.BROADCAST_ACTION).apply {
                        putExtra(EXTRA_SONG, myMediaPlayer.currentSong)
                    })
                    stopForeground(true)
                    stopSelf()
                }

                else -> {
                    throw IllegalArgumentException("Illegal action $action")
                }
            }
        }
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }


    companion object {

        var isActive = false

        internal const val EXTRA_SONG = "extraSong"
        private const val ACTION_PLAY: String = "com.pustovit.hellowword.PLAY"
        private const val ACTION_PAUSE: String = "com.pustovit.hellowword.PAUSE"
        private const val ACTION_STOP: String = "com.pustovit.hellowword.STOP"
        private const val PACKAGE_NAME: String = "com.pustovit.hellowword"


        fun getIntentPlaySong(song: Song): Intent {
            return Intent(ACTION_PLAY).apply {
                putExtra(EXTRA_SONG, song)
                setPackage(PACKAGE_NAME)
            }
        }


        fun getIntentPauseSong(): Intent {
            return Intent(ACTION_PAUSE).apply {
                setPackage(PACKAGE_NAME)
            }
        }

        fun getIntentStopSong(): Intent {
            return Intent(ACTION_STOP).apply {
                setPackage(PACKAGE_NAME)
            }
        }

    }
}