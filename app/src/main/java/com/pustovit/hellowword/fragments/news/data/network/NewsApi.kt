package com.pustovit.hellowword.fragments.news.data.network

import androidx.lifecycle.LiveData
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {

    /**
     * TODO
     *
     * @param query
     * @param from A date and optional time for the oldest article allowed.
     *              This should be in ISO 8601 format (e.g. 2020-09-29 or 2020-09-29T12:22:31)
     *              Default: the oldest according to your plan.
     * @param apiKey
     * @return
     */
    @GET("/v2/everything")
    fun getNews(
        @Query("q") query: String,
        @Query("from") from: String,
        @Query("to") to: String,
        @Query("apiKey") apiKey: String
    ): Call<NewsResponsePage>


    @GET("/v2/everything")
    fun getNewsLiveData(
        @Query("q") query: String,
        @Query("from") from: String,
        @Query("to") to: String,
        @Query("apiKey") apiKey: String
    ): LiveData<ApiResponse<NewsResponsePage>>

    @GET("/v2/everything")
    fun getNewsObservable(
        @Query("q") query: String,
        @Query("from") from: String,
        @Query("to") to: String,
        @Query("apiKey") apiKey: String
    ): Observable<NewsResponsePage>
}
