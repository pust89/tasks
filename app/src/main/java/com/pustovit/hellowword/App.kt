package com.pustovit.hellowword

import android.app.Application
import com.pustovit.hellowword.fragments.news.di.core.AppComponent
import com.pustovit.hellowword.fragments.news.di.core.DaggerAppComponent
import com.pustovit.hellowword.fragments.news.di.news.NewsComponent
import com.pustovit.hellowword.navigation.NavigatorHolderProvider
import com.pustovit.hellowword.navigation.RouterProvider
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import timber.log.Timber


class App : Application(), RouterProvider, NavigatorHolderProvider {

    companion object {
        const val LOG_TAG = "myLog"
        const val PREFERENCE_KEY = "PREFERENCE_KEY"
    }


    val appComponent: AppComponent by lazy {
        DaggerAppComponent.factory().create(applicationContext)
    }




    private lateinit var cicerone: Cicerone<Router>
    override fun onCreate() {
        super.onCreate()
        cicerone = Cicerone.create();
        setupTimber()
    }

    override fun getNavigatorHolder(): NavigatorHolder {
        return cicerone.navigatorHolder
    }

    override fun getRouter(): Router {
        return cicerone.router
    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}