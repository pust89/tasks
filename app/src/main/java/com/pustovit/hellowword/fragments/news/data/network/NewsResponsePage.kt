package com.pustovit.hellowword.fragments.news.data.network

import com.pustovit.hellowword.fragments.news.data.database.DatabaseNews
import com.pustovit.hellowword.fragments.news.domain.News


data class NewsResponsePage(
    val articles: List<Article> = listOf(),
    val status: String = "",
    val totalResults: Int = 0
)


data class Article(
    val author: String?,
    val content: String?,
    val description: String?,
    val publishedAt: String?,
    val source: Source?,
    val title: String?,
    val url: String?,
    val urlToImage: String?
)


data class Source(
    val name: String = ""
)


fun List<Article>.asDomainModel():List<News>{
    return this.map {
        News(
            author = it.author.orEmpty(),
            content = it.content.orEmpty(),
            description = it.description.orEmpty(),
            publishedAt = it.publishedAt?.substring(0,10).orEmpty(),
            sourceName = it.source?.name.orEmpty(),
            title = it.title.orEmpty(),
            url = it.url.orEmpty(),
            urlToImage = it.urlToImage
        )
    }
}

fun List<Article>.asDatabaseModel(query:String):List<DatabaseNews>{
    return this.map {
        DatabaseNews(
            query = query,
            author = it.author.orEmpty(),
            content = it.content.orEmpty(),
            description = it.description.orEmpty(),
            publishedAt = it.publishedAt?.substring(0,10).orEmpty(),
            sourceName = it.source?.name.orEmpty(),
            title = it.title.orEmpty(),
            url = it.url.orEmpty(),
            urlToImage = it.urlToImage
        )
    }
}
