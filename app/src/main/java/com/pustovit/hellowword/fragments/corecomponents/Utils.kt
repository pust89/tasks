package com.pustovit.hellowword.fragments.corecomponents

import android.content.ContentValues
import android.database.Cursor
import com.pustovit.hellowword.fragments.corecomponents.contentprovider.SongsContract
import com.pustovit.hellowword.fragments.corecomponents.contentprovider.database.entity.DatabaseSong
import java.util.concurrent.Executors

fun Cursor.asListSong(): List<Song> {
    val songs: MutableList<Song> = ArrayList()
    if (this.count > 0) {
        if (this.moveToFirst()) {
            do {
                val song = Song(
                    _id = this.getLong(this.getColumnIndex(SongsContract.COLUMN_ID)),
                    title = this.getString(this.getColumnIndex(SongsContract.COLUMN_TITLE)),
                    resourceId = this.getInt(this.getColumnIndex(SongsContract.COLUMN_RESOURCE_ID)),
                    artist = this.getString(this.getColumnIndex(SongsContract.COLUMN_ARTIST)),
                    genre = this.getString(this.getColumnIndex(SongsContract.COLUMN_GENRE)),
                    currentPosition = 0
                ).apply {
                    songs.add(this)
                }
            } while (this.moveToNext())

        }
    }
    return songs.toList()
}

fun ContentValues.asDatabaseSong(): DatabaseSong {
    val databaseSong = DatabaseSong(
        resourceId = this.getAsInteger(SongsContract.COLUMN_RESOURCE_ID),
        title = this.getAsString(SongsContract.COLUMN_TITLE),
        artist = this.getAsString(SongsContract.COLUMN_ARTIST),
        genre = this.getAsString(SongsContract.COLUMN_GENRE)
    )
    if (this.containsKey(SongsContract.COLUMN_ID)) {
        databaseSong._id = this.getAsLong(SongsContract.COLUMN_ID)
    }
    return databaseSong
}

fun DatabaseSong.asContentValues(): ContentValues {
    val cv: ContentValues = ContentValues()
    cv.put(SongsContract.COLUMN_ID, this._id)
    cv.put(SongsContract.COLUMN_RESOURCE_ID, this.resourceId)
    cv.put(SongsContract.COLUMN_TITLE, this.title)
    cv.put(SongsContract.COLUMN_ARTIST, this.artist)
    cv.put(SongsContract.COLUMN_GENRE, this.genre)
    return cv
}

private val IO_EXECUTOR = Executors.newSingleThreadExecutor()

/**
 * Utility method to run blocks on a dedicated background thread, used for io/database work.
 */
fun ioThread(f: () -> Unit) {
    IO_EXECUTOR.execute(f)
}