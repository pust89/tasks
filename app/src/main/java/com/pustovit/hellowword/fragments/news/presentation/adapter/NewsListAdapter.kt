package com.pustovit.hellowword.fragments.news.presentation.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.pustovit.hellowword.R
import com.pustovit.hellowword.fragments.corecomponents.inflate
import com.pustovit.hellowword.fragments.news.domain.News
import kotlinx.android.synthetic.main.item_news.view.*

class NewsListAdapter(private val newsItemClickListener: NewsItemClickListener<News>) :
    ListAdapter<News, NewsListAdapter.NewsVh>(NewsDiffUtilItemCallback()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsVh {
        return NewsVh(parent.inflate(R.layout.item_news))
    }

    override fun onBindViewHolder(holder: NewsVh, position: Int) {
        holder.bind(getItem(position),newsItemClickListener)
    }

    class NewsVh(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(news: News, newsItemClickListener: NewsItemClickListener<News>) {
                view.apply {
                    item_tv_news_title.text = news.title
                    item_tv_news_author.text = news.author
                    item_tv_news_publishedAt.text = news.publishedAt
                    setOnClickListener {
                        newsItemClickListener.onClick(news)
                    }
                }

        }
    }
}