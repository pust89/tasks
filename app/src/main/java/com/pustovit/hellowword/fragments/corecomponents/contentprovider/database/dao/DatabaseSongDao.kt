package com.pustovit.hellowword.fragments.corecomponents.contentprovider.database.dao

import android.database.Cursor
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteQuery
import com.pustovit.hellowword.fragments.corecomponents.contentprovider.database.entity.DatabaseSong


@Dao
interface DatabaseSongDao {

    @RawQuery
    fun query(query: SupportSQLiteQuery): Cursor

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDatabaseSong(databaseSong: DatabaseSong): Long

    @RawQuery
    fun delete(query: SupportSQLiteQuery): Int

    @Query("DELETE FROM songs WHERE _id =:id;")
    fun deleteById(id: Long): Int

    @RawQuery
    fun update(query: SupportSQLiteQuery): Int

    @Update
    fun updateDatabaseSongs(databaseSong: DatabaseSong):Int

}