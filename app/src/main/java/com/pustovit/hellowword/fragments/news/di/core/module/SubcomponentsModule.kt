package com.pustovit.hellowword.fragments.news.di.core.module

import com.pustovit.hellowword.fragments.news.di.news.NewsComponent
import dagger.Module

@Module(subcomponents = [NewsComponent::class])
class SubcomponentsModule {
}